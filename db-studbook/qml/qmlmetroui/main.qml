import QtQuick 1.1
import Bridge 1.0
import "../qmlmetroui/pages/"
import "../qmlmetroui/components"

Rectangle {
    id: root // base for all items
    width: bridge.screeneWidth() // screen taking screen parametres
    height: bridge.screeneHeight()
    color: "grey"
    state: "mainPage_active"

    Bridge{
        id: bridge // object to take data from c++
    }

    Behavior on color { // changing pages color animation
        ColorAnimation {duration: 200 }
    }

    LeftMenu{ // adding left menu
        id: leftMenu
    }

    StartPage{ // start page
        id: startPg
        x: bridge.screeneWidth()
        y: 10
        opacity: 0
        width: root.width - leftMenu.width - 10 * 2
        height: root.height - quitButton.height - 10 * 2
    }

    LecturePage{ // pages with lectures
        id: lecturePg
        x: bridge.screeneWidth()
        y: 10
        opacity: 0
        width: root.width - leftMenu.width - 10 * 2
        height: root.height - quitButton.height - 10 * 2
    }

    PracticePage{ // pages with paractice
        id: practicePg
        x: bridge.screeneWidth()
        y: 10
        opacity: 0
        width: root.width - leftMenu.width - 10 * 2
        height: root.height - quitButton.height - 10 * 2
    }

    StandAlonePage{ // page with stand alone work on material
        id: alonePg
        x: bridge.screeneWidth()
        y: 10
        opacity: 0
        width: root.width - leftMenu.width - 10 * 2
        height: root.height - quitButton.height - 10 * 2
    }

    StatisticsPage{ // pages with statistics. Do I need it?
        id: statsPg
        x: bridge.screeneWidth()
        y: 10
        opacity: 0
        width: root.width - leftMenu.width - 10 * 2
        height: root.height - quitButton.height - 10 * 2
    }

    MetroToolButton{ // quit button
        id: quitButton
        x: root.width - quitButton.width - 10
        y: root.height - quitButton.height - 10
        headerText: qsTr("Выход")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: quitButton
            onClicked: Qt.quit()
        }
    }

    states: [
        State {
            name: "mainPage_active"
            PropertyChanges {
                target: startPg
                x: leftMenu.width + 10
                y: 10
                opacity: 1
            }
        },

        State {
            name: "lecturePage_active"
            PropertyChanges {
                target: lecturePg
                x: leftMenu.width + 10
                y: 10
                opacity: 1
            }
        },

        State {
            name: "practicePage_active"
            PropertyChanges {
                target: practicePg
                x: leftMenu.width + 10
                y: 10
                opacity: 1
            }
        },

        State {
            name: "alonePage_active"
            PropertyChanges {
                target: alonePg
                x: leftMenu.width + 10
                y: 10
                opacity: 1
            }
        },

        State {
            name: "statsPage_active"
            PropertyChanges {
                target: statsPg
                x: leftMenu.width + 10
                y: 10
                opacity: 1
            }
        }
    ]

    transitions: [ // changing pages
        Transition {
            from: "mainPage_active"
            to: "lecturePage_active"
            reversible: true
            PropertyAnimation {
                target: startPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: lecturePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "practicePage_active"
            to: "lecturePage_active"
            reversible: true
            PropertyAnimation {
                target: startPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: lecturePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "alonePage_active"
            to: "lecturePage_active"
            reversible: true
            PropertyAnimation {
                target: startPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: lecturePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "statsPage_active"
            to: "lecturePage_active"
            reversible: true
            PropertyAnimation {
                target: startPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: lecturePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "mainPage_active"
            to: "practicePage_active"
            reversible: true
            PropertyAnimation {
                target: startPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: practicePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "alonePage_active"
            to: "practicePage_active"
            reversible: true
            PropertyAnimation {
                target: alonePg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: practicePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "statsPage_active"
            to: "practicePage_active"
            reversible: true
            PropertyAnimation {
                target: statsPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: practicePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "mainPage_active"
            to: "alonePage_active"
            reversible: true
            PropertyAnimation {
                target: startPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: alonePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "statsPage_active"
            to: "alonePage_active"
            reversible: true
            PropertyAnimation {
                target: statsPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: alonePg
                properties: "x, y, opacity"
                duration: 500
            }
        },

        Transition {
            from: "mainPage_active"
            to: "statsPage_active"
            reversible: true
            PropertyAnimation {
                target: startPg
                properties: "x, y, opacity"
                duration: 500
            }
            PropertyAnimation {
                target: statsPg
                properties: "x, y, opacity"
                duration: 500
            }
        }
    ]
}





