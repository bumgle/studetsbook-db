import QtQuick 1.1
import "../qmlmetroui/components"

Item {
    property int originX: 0
    property int originY: 0

    width: button1.width + 10 * 2
    height: parent.height

    Behavior on x{
        NumberAnimation{duration: 200}
    }

    Component.onCompleted: {
        originX = parent.x
        originY = parent.y
    }

    MetroButton{
        id: button1
        buttonInGroupe: 1
        headerText: qsTr("Лекции")
        buttonIcon: "qrc:/pics/pictures/thumbnails.png"
        x:10
        y: parent.height -  380

        onClickRecived: {
            mainButtonsMove(button1.buttonInGroupe)
        }
    }

    MetroButton{
        id:button2
        buttonInGroupe: 2
        headerText: qsTr("Практические <br>занятия")
        buttonIcon: "qrc:/pics/pictures/users.png"
        x: 10
        y: parent.height -  285
        btnColor: "red"

        onClickRecived: {
            mainButtonsMove(button2.buttonInGroupe)
        }
    }

    MetroButton{
        id:button3
        buttonInGroupe: 3
        headerText: qsTr("Задания для <br>самоконтроля")
        buttonIcon: "qrc:/pics/pictures/edit.png"
        x: 10
        y: parent.height -  190
        btnColor: "orange"

        onClickRecived: {
            mainButtonsMove(button3.buttonInGroupe)
        }
    }

    MetroButton{
        id:button4
        buttonInGroupe: 4
        headerText: qsTr("Статистика")
        buttonIcon: "qrc:/pics/pictures/statistics.png"
        x: 10
        y: parent.height -  95
        btnColor: "green"

        onClickRecived: {
            mainButtonsMove(button4.buttonInGroupe)

        }
    }

    function mainButtonsMove(id){
        switch (id){
        case 1:
            if (button1.triggeredstate == true){
                button1.x = 10
                button1.y = 10
                parent.color = button1.color

                button2.triggeredstate = false
                button3.triggeredstate = false
                button4.triggeredstate = false

                root.state = "lecturePage_active"
            }
            else{
                parent.color = "grey"
                root.state = "mainPage_active"
            }

            break

        case 2:
            if (button2.triggeredstate == true){
                button2.x = 10
                button2.y = 10
                parent.color = button2.color

                button1.triggeredstate = false
                button1.x = button2.originX
                button1.y = button2.originY
                button3.triggeredstate = false
                button4.triggeredstate = false

                root.state = "practicePage_active"
            }
            else{
                parent.color = "grey"
                button1.x = button1.originX
                button1.y = button1.originY
                root.state = "mainPage_active"
            }
            break

        case 3:
            if (button3.triggeredstate == true){
                button3.x = 10
                button3.y = 10
                parent.color = button3.color

                button1.triggeredstate = false
                button1.x = button2.originX
                button1.y = button2.originY
                button2.triggeredstate = false
                button2.x = button3.originX
                button2.y = button3.originY
                button4.triggeredstate = false
                root.state = "alonePage_active"
            }
            else{
                parent.color = "grey"
                button1.x = button1.originX
                button1.y = button1.originY
                button2.x = button2.originX
                button2.y = button2.originY
                button4.x = button4.originX
                button4.y = button4.originY
                root.state = "mainPage_active"
            }

            break
        case 4:
            if (button4.triggeredstate == true){
                button4.x = 10
                button4.y = 10
                parent.color = button4.color

                button1.triggeredstate = false
                button1.x = button2.originX
                button1.y = button2.originY
                button2.triggeredstate = false
                button2.x = button3.originX
                button2.y = button3.originY
                button3.triggeredstate = false
                button3.x = button4.originX
                button3.y = button4.originY
                root.state = "statsPage_active"
            }
            else{
                parent.color = "grey"
                button1.x = button1.originX
                button1.y = button1.originY
                button2.x = button2.originX
                button2.y = button2.originY
                button3.x = button3.originX
                button3.y = button3.originY
                root.state = "mainPage_active"
            }

            break
        }
    }
}
