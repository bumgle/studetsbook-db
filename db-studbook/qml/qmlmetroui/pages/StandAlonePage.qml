import QtQuick 1.1
import Bridge 1.0
import CustomComponents 1.0
import "pagescripts.js" as PageScripts

Item{
    property int originX
    property int originY

    id: aloneBase

    Database{
        id: db
    }

    Behavior on opacity{
        NumberAnimation{duration: 200}
    }

    Text{
        id: bookName
        x: 10
        y: 25
        text: qsTr("Самостоятельная робота")
        color: "white"
        font.family: "Monaco"
        font.bold: true
        font.pixelSize: 50
    }

    Line{
        id: separaror
        x1: 0
        y1: 100
        x2: root.width - leftMenu.width - 10 * 3
        y2: 100
        color: "white"
        penWidth: 2
    }

    ListModel{
        id: lectures

    }

    GridView{
        anchors.top: separaror.bottom
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        clip: true

        model: lectures
        delegate: LessonItem{
            headerText: head;
            bodyText: body;
            ballValue: ball
            view: v
            status: s
        }
        cellWidth: 200 + 20
        cellHeight: 125 + 20


    }

    Component.onCompleted: {
        aloneBase.originX = aloneBase.x
        aloneBase.originY = aloneBase.y
        db.initDatabase()
        PageScripts.refreshSelfJobModel()
    }

}

