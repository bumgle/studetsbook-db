import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "orange"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Самомтоятельная работа 1</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Групповые операции</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\tСамостоятельно изучите материал о транзакциях и контрольных точках."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 40
                    }

                   Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                   }

                   Text{
                        text: "<b>Задания</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                   }

                   // zad 1
                   LessonSingleQuestion{
                       id: q1
                       question : "Что такое транзакция?"
                   }

                   SingleQuestionVariant{
                       id: q1v1
                       variantText: "Передача данных."
                   }

                   SingleQuestionVariant{
                       id: q1v2
                       variantText: "Последовательное выполнение группы запроссов." //+
                   }

                   SingleQuestionVariant{
                       id: q1v3
                       variantText: "Определенный вид промежуточных таблиц."
                   }


                   // zad 2
                   LessonSingleQuestion{
                       id: q2
                       question : "Какая команда запускает выполнение транзакции?"
                   }

                   SingleQuestionVariant{
                       id: q2v1
                       variantText: "START"
                   }

                   SingleQuestionVariant{
                       id: q2v2
                       variantText: "START_TRANSACTION"
                   }

                   SingleQuestionVariant{
                       id: q2v3
                       variantText: "COMMIT" // +
                   }

                   SingleQuestionVariant{
                       id: q2v4
                       variantText: "EXECUTE"
                   }


                   // zad 3

                   LessonSingleQuestion{
                       id: q3
                       question : "Для чего предназначена команда SAVEPOINT?"
                   }

                   SingleQuestionVariant{
                       id: q3v1
                       variantText: "Для создания контрольной точки состояния базы данных." // +
                   }

                   SingleQuestionVariant{
                       id: q3v2
                       variantText: "Для сохранения изменений в базе данных."
                   }

                   SingleQuestionVariant{
                       id: q3v3
                       variantText: "Для создания резервной копии базы данных."
                   }

                   // zad 4

                   LessonSingleQuestion{
                       id: q4
                       question : "Какая команда возвращает состояние базы данных к контрольной точке?"
                   }

                   SingleQuestionVariant{
                       id: q4v1
                       variantText: "RETURN"
                   }

                   SingleQuestionVariant{
                       id: q4v2
                       variantText: "REQUEST"
                   }

                   SingleQuestionVariant{
                       id: q4v3
                       variantText: "ROLL"
                   }

                   SingleQuestionVariant{
                       id: q4v4
                       variantText: "ROLLBACK" //+
                   }


                   Text{
                        text: "<br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_self2()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "orange"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
