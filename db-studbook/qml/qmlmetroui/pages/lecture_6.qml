import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "#04A7FF"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Лекция 6</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Подзапросы</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>КАК РАБОТАЕТ ПОДЗАПРОС?</b><br><br>\tС помощью SQL вы можете вкладывать запросы внутрь друга друга. Обычно, внутренний запрос генерирует значение которое проверяется в предикате внешнего запроса, определяющего верно оно или нет. Например, предположим что мы знаем им продавца: Motika, но не знаем значение его пол snum, и хотим извлечь все порядки из таблицы Порядков. Имеется один способ чтобы сделать это."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }


                    CodeExample{
                        exampleText: "<b>SELECT * FROM Orders WHERE snum = ( SELECT snum FROM Salespeople WHERE sname = 'Motika');</b> "
                    }


                    Text{
                        text: "\tЧтобы оценить внешний( основной ) запрос, SQL сначала должен оценить внутренний запрос ( или подзапрос ) внутри предложения WHERE. Он делает это так как и должен делать запрос имеющий единственную цель - отыскать через таблицу Продавцов все строки, где поле sname равно значению Motika, и затем извлечь значения пол snum этих строк. Единственной найденной строкой естественно будет snum = 1004. Однако SQL, не просто выдает это значение, а помещает его в предикат основного запроса вместо самого подзапроса, так чтобы предиката прочитал что"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>WHERE snum = 1004 </b>"
                    }

                    Text{
                        text: "\tОсновной запрос затем выполняется как обычно с вышеупомянутыми результатами. Конечно же, подзапрос должен выбрать один и только один столбец, а тип данных этого столбца должен совпадать с тем значением с которым он будет сравниваться в предикате. Часто, как показано выше, выбранное поле и его значение будут иметь одинаковые имена( в этом случае, snum ), но это необязательно.<br>\tКонечно, если бы мы уже знали номер продавца Motika, мы могли бы просто напечатать WHERE snum = 1004 и выполнять далее с подзапросом в целом, но это было бы не так универсально. Это будет продолжать работать даже если номер Motika изменился, а, с помощью простого изменения имени в подзапросе, вы можете использовать его для чего угодно.<br><br><b>ЗНАЧЕНИЯ, КОТОРЫЕ МОГУТ ВЫДАВАТЬ ПОДЗАПРОСЫ</b><br><br>\tСкорее всего было бы удобнее, чтобы наш подзапрос в предыдущем примере возвращал одно и только одно значение.<br>\tИмея выбранным поле snum <b>WHERE city = 'London'</b> вместо <b>'WHERE sname = 'Motika'</b>, можно получить несколько различных значений. Это может сделать уравнение в предикате основного запроса невозможным для оценки верности или неверности, и команда выдаст ошибку.<br>\tПри использовании подзапросов в предикатах основанных на реляционных операторах, вы должны убедиться что использовали подзапрос который будет выдавать одну и только одну строку вывода. Если вы используете подзапрос который не выводит никаких значений вообще, команда не потерпит неудачи; но основной запрос не выведет никаких значений. Подзапросы которые не производят никакого вывода (или нулевой вывод) вынуждают рассматривать предикат ни как верный ни как неверный, а как неизвестный. Однако, неизвестный предикат имеет тот же самый эффект что и неверный: никакие строки не выбираются основным запросом.<br>\tЭто плоха стратеги, чтобы делать что-нибудь подобное следующему:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM Orders WHERE snum = ( SELECT snum FROM Salespeople WHERE city = Barcelona );</b>"
                    }

                    Text{
                        text: "\tПоскольку мы имеем только одного продавца в Barcelona - Rifkin, то подзапрос будет выбирать одиночное значение snum и следовательно будет принят. Но это - только в данном случае. Большинство SQL баз данных имеют многочисленных пользователей, и если другой пользователь добавит нового продавца из Barcelona в таблицу, подзапрос выберет два значения, и ваша команда потерпит неудачу.<br><br><b>DISTINCT С ПОДЗАПРОСАМИ<b><br><br>\tВы можете, в некоторых случаях, использовать DISTINCT чтобы вынудить подзапрос генерировать одиночное значение. Предположим что мы хотим найти все порядки кредитований для тех продавцов которые обслуживают Hoffmanа ( cnum = 2001 ).<br>\tИмеется один способ чтобы сделать это:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "SELECT * FROM Orders WHERE snum = ( SELECT DISTINCT snum FROM Orders WHERE cnum = 2001 ); "
                    }

                    Text{
                        text: "\tПодзапрос установил что значение пол snum совпало с Hoffman - 1001, и затем основной запрос выделил все порядки с этим значением snum из таблицы Порядков( не разбирая, относятся они к Hoffman или нет). Так как каждый заказчик назначен к одному и только этому продавцу, мы знаем что каждая строка в таблице Порядков с данным значением cnum должна иметь такое же значение snum. Однако так как там может быть любое число таких строк, подзапрос мог бы вывести много ( хотя и идентичных ) значений snum для данного пол cnum. Аргумент <b>DISTINCT</b> предотвращает это. Если наш подзапрос возвратит более одного значения, это будет указывать на ошибку в наших данных - хороша вещь для знающих об этом.<br>\tАльтернативный подход должен быть чтобы ссылаться к таблице Заказчиков а не к таблице Порядков в подзапросе. Так как поле cnum - это первичный ключ таблицы Заказчика, запрос выбирающий его должен произвести только одно значение. Это рационально только если вы как пользователь имеете доступ к таблице Порядков но не к таблице Заказчиков. В этом случае, вы можете использовать решение которое мы показали выше. Пожалуйста учтите, что методика используемая в предшествующем примере применима только когда вы знаете, что два различных пол в таблице должны всегда совпадать, как в нашем случае. Эта ситуация не является типичной в реляционных базах данных, она является исключением из правил.<br><br><b>ПРЕДИКАТЫ С ПОДЗАПРОСАМИ ЯВЛЯЮТСЯ НЕОБРАТИМЫМИ</b><br><br>\tВы должны обратить внимание что предикаты включающие подзапросы, используют <b>выражение - скалярная форма - оператор - подзапрос</b>, а, не <b>подзапрос - оператор - скалярное выражение</b> или, <b>подзапрос - оператор - подзапрос</b>. Другими словами, вы не должны записывать предыдущий пример так:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM Orders WHERE ( SELECT DISTINCT snum FROM Orders WHERE cnum = 2001 ) = snum;</b> "
                    }

                    Text{
                        text: "/tВ строгой ANSI реализации, это приведет к неудаче, хотя некоторые программы и позволяют делать такие вещи. ANSI также предохраняет вас от появления обеих значений при сравнении, которые нужно вывести с помощью подзапроса.<br><br><b>ИСПОЛЬЗОВАНИЕ АГРЕГАТНЫХ ФУНКЦИЙ В ПОДЗАПРОСАХ</b><br><br>\tОдин тип функций, который автоматически может производить одиночное значение для любого числа строк, конечно же, - агрегатная функция.<br>\tЛюбой запрос использующий одиночную функцию агрегата без предложения GROUP BY будет выбирать одиночное значение для использования в основном предикате. Например, вы хотите увидеть все порядки имеющие сумму приобретений выше средней на 4-е Октября:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM Orders WHERE amt > ( SELECT AVG (amt) FROM Orders WHERE odate = 10/04/1990 ); </b>"
                    }

                    Text{
                        text: "\tИмейте ввиду что сгруппированные агрегатные функции, которые являются агрегатными функциями определенными в терминах предложения <b>GROUP BY</b>, могут производить многочисленные значения. Они, следовательно, не позволительны в подзапросах такого характера. Даже если <b>GROUP BY</b> и <b>HAVING</b> используются таким способом, что только одна группа выводится с помощью подзапроса, команда будет отклонена в принципе. Вы должны использовать одиночную агрегатную функцию с предложением <b>WHERE</b> что устранит нежелательные группы. Например, следующий запрос который должен найти сред- нее значение комиссионных продавца в Лондоне -"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT AVG (comm) FROM Salespeople GROUP BY city HAVlNG city = London;</b>"
                    }

                    Text{
                        text: "не может использоваться в подзапросе! Во всяком случае это не лучший способ формировать запрос. Другим способом может быть -"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT AVG (comm) FROM Salespeople WHERE city = London; </b>"
                    }

                    Text{
                        text: "<br><b>ИСПОЛЬЗОВАНИЕ ПОДЗАПРОСОВ КОТОРЫЕ ВЫДАЮТ МНОГО СТРОК С ПОМОЩЬЮ ОПЕРАТОРА IN</b><br><br>\tВы можете использовать подзапросы которые производят любое число строк если вы используете специальный оператор <b>IN</b> ( операторы <b>BETWEEN, LIKE</b>, и <b>IS NULL</b> не могут использоваться с подзапросами ). Как вы помните, IN определяет набор значений, одно из которых должно совпадать с другим термином уравнения предиката в порядке, чтобы предикат был верным. Когда вы используете <b>IN</b> с подзапросом, SQL просто формирует этот набор из вывода подзапроса. Мы можем, следовательно, использовать <b>IN</b> чтобы выполнить такой же подзапрос который не будет работать с реляционным оператором, и найти все атрибуты таблицы Порядков для продавца в Лондоне :"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM Orders WHERE snum IN ( SELECT snum FROM Salespeople WHERE city = LONDON );</b> "
                    }

                    Text{
                        text: "\tВ ситуации подобно этой, подзапрос - более прост для пользователя чтобы понимать его и более прост для компьютера чтобы его выполнить, чем если бы Вы использовали объединение:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT onum, amt, odate, cnum, Orders.snum FROM Orders, Salespeople WHERE Orders.snum = Salespeople.snum AND Salespeople.city = London;</b>"
                    }

                    Text{
                        text: "\tХотя это и произведет тот же самый вывод что и в примере с подзапросом, SQL должен будет просмотреть каждую возможную комбинацию строк из двух таблиц и проверить их снова по составному предикату. Проще и эффективнее извлекать из таблицы Продавцов значения пол snum где city = London, и затем искать эти значения в таблице Порядков, как это делается в варианте с подзапросом. Внутренний запрос дает нам snums=1001 и snum=1004. Внешний запрос, затем, дает нам строки из таблицы Порядков где эти пол snum найдены. Строго говор, быстрее или нет работает вариант подзапроса, практически зависит от реализации - в какой программе вы это используете. Эта часть вашей программы называемой - оптимизатор, пытается найти наиболее эффективный способ выполнения ваших запросов.<br>\tХороший оптимизатор во всяком случае преобразует вариант объединения в подзапрос, но нет достаточно простого способа для вас чтобы выяснить выполнено это или нет. Лучше сохранить ваши запросы в памяти чем полагаться полностью на оптимизатор.<br>\tКонечно вы можете также использовать оператор <b>IN</b>, даже когда вы уверены что подзапрос произведет одиночное значение. В любой ситуации где вы можете использовать реляционный оператор сравнения (=), вы можете использовать <b>IN</b>. В отличие от реляционных операторов, IN не может заставить команду потерпеть неудачу если больше чем одно значение выбрано подзапросом. Это может быть или преимуществом или недостатком. Вы не увидите непосредственно вывода из подзапросов; если вы полагаете что подзапрос собирается произвести только одно значение, а он производит различные. Вы не сможете объяснить различи в выводе основного запроса. Например, рассмотрим команду, которая похожа на предыдущую:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT onum, amt, odate FROM Orders WHERE snum = ( SELECT  snum FROM Orders WHERE cnum = 2001 ); </b>"
                    }

                    Text{
                        text: "\tВы можете устранить потребность в DISTINCT используя IN вместо (=), подобно этому:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT onum, amt, odate FROM Orders WHERE snum IN ( SELECT snum FROM Orders WHERE cnum = 2001 );</b> "
                    }

                    Text{
                        text: "\tЧто случится если есть ошибка и один из порядков был аккредитован к различным продавцам? Версия использующая IN будет давать вам все порядки для обоих продавцов. Нет никакого очевидного способа наблюдения за ошибкой, и поэтому сгенерированные отчеты или решения сделанные на основе этого запроса не будут содержать ошибки. Вариант использующий ( = ) , просто потерпит неудачу.<br>\tЭто, по крайней мере, позволило вам узнать что имеется такая проблема. Вы должны затем выполнять поиск неисправности, выполнив этот подза- прос отдельно и наблюдая значения которые он производит.<br>\tВ принципе, если вы знаете что подзапрос должен( по логике) вывести только одно значение, вы должны использовать = . IN является подходящим, если запрос может ограниченно производить одно или более значений, независимо от того ожидаете вы их или нет. Предположим, мы хотим знать комиссионные всех продавцов обслуживаю- щих заказчиков в Лондоне:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT comm FROM Salespeople WHERE snum IN ( SELECT snum FROM Customers WHERE city = London );</b>"
                    }

                    Text{
                        text: "<br><b>ПОДЗАПРОСЫ ВЫБИРАЮТ ОДИНОЧНЫЕ СТОЛБЦЫ</b><br><br>\tСмысл всех подзапросов обсужденных в этой главе тот, что все они выбирают одиночный столбец. Это обязательно, поскольку выбранный вывод сравнивает- с одиночным значением. Подтверждением этому то, что <b>SELECT *<\b> не может использоваться в подзапросе.<br>\tВы можете использовать выражение основанное на столбце, а не просто сам столбец, в предложении <b>SELECT</b> подзапроса. Это может быть выполнено или с помощью реляционных операторов или с <b>IN</b>. Например, следующий запрос использует реляционный оператор =:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM Customers WHERE cnum = ( SELECT snum + 1000 FROM Salespeople WHERE sname = Serres );</b>"
                    }

                    Text{
                        text: "\tОн находит всех заказчиков чье значение пол cnum равное 1000, выше пол snum Serres. Мы предполагаем что столбец sname не имеет никаких двойных значений; иначе подзапрос может произвести многочисленные значения. Когда пол snum и сnum не имеют такого простого функционального значения как например первичный ключ , что не всегда хорошо, запрос типа вышеупомянутого невероятно полезен."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                   Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Контрольные вопросы:</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    // question 1
                    LessonSingleQuestion{
                        id: q1
                        question : "Каково предназначение подзапросов?"
                    }

                    SingleQuestionVariant{
                        id: q1v1
                        variantText: "Внутренние подзапросы геннирируют значения которые подставляються в исполняемый запрос."// *
                    }
                    SingleQuestionVariant{
                        id: q1v2
                        variantText: "Уточнение данных."
                    }
                    SingleQuestionVariant{
                        id: q1v3
                        variantText: "Установка условия выборки."
                    }
                    SingleQuestionVariant{
                        id: q1v4
                        variantText: "Установка сложного условия выборки."
                    }

                    // question 2
                    LessonSingleQuestion{
                        id: q2
                        question : "Являються предикаты с подзапросами обратимыми?"
                    }

                    SingleQuestionVariant{
                        id: q2v1
                        variantText: "Да"
                    }
                    SingleQuestionVariant{
                        id: q2v2
                        variantText: "Нет"// *
                    }

                    // question 3
                    LessonSingleQuestion{
                        id: q3
                        question : "Допускаеться ли испльзование DISTINCT в подзапросе?"
                    }

                    SingleQuestionVariant{
                        id: q3v1
                        variantText: "Да"
                    }
                    SingleQuestionVariant{
                        id: q3v2
                        variantText: "Нет"// *
                    }

                    // question 4
                    LessonSingleQuestion{
                        id: q4
                        question : "Какое количество столбцов должен возвращать подзапрос"
                    }

                    SingleQuestionVariant{
                        id: q4v1
                        variantText: "0"
                    }
                    SingleQuestionVariant{
                        id: q4v2
                        variantText: "1"// *
                    }
                    SingleQuestionVariant{
                        id: q4v3
                        variantText: "2"
                    }
                    SingleQuestionVariant{
                        id: q4v4
                        variantText: "Не ограничено."
                    }
                    // question 5

                    LessonSingleQuestion{
                        id: q5
                        question : "Какими операторами инициализируеться подзапрос?"
                    }

                    SingleQuestionVariant{
                        id: q5v1
                        variantText: "AS"
                    }
                    SingleQuestionVariant{
                        id: q5v2
                        variantText: "="// *
                    }
                    SingleQuestionVariant{
                        id: q5v3
                        variantText: "IN" //*
                    }
                    SingleQuestionVariant{
                        id: q5v4
                        variantText: "IS"
                    }


                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_lecture6()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "#04A7FF"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
