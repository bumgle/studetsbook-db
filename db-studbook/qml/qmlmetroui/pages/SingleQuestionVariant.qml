// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {

    property int variantId: 0
    property string variantText: "Variant"
    property bool checkState: false

    width: parent.width - 20
    height: txt.paintedHeight + txt.font.pointSize + 5
    color: "lightgrey"

    Image {
        id: imgButton
        source: "qrc:/pics/pictures/basecircle.png"
        width: 20
        height: 20
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.top: parent.top
        anchors.topMargin: 15

        MouseArea{
            id: imgButtonMouseArea
            anchors.fill: imgButton
            onClicked: {
                if (!checkState){
                    imgButton.source = "qrc:/pics/pictures/check.png"
                    checkState = true
                }
                else{
                    imgButton.source = "qrc:/pics/pictures/basecircle.png"
                    checkState = false
                }
            }
        }

    }

    Text{
        id: txt
        anchors.left: imgButton.right
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        text: variantText
        color: "black"
        width: parent.width - 30 - imgButton.width
        wrapMode: Text.WordWrap
        textFormat: Text.StyledText
        horizontalAlignment: Text.AlignJustify
        font.pixelSize: 20

    }
}
