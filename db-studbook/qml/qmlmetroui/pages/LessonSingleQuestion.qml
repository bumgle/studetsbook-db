import QtQuick 1.1

Item {
    id: singleQuestionBase

    property string question : "Question Text"
    property int questionId;
    property variant variantsParametrees;

    width: parent.width - 20
    height: questionTextBox.height

    Rectangle{
        id: questionTextBox;
        anchors.fill: parent
        width: parent.width
        height: questionText.paintedHeight + questionText.font.pointSize - 2
        color: "darkgrey"


        Text{
            id: questionText
            text: question
            anchors.top: questionTextBox.top
            anchors.topMargin: 5
            anchors.left: questionTextBox.left
            anchors.leftMargin: 10
            color: "black"
            width: singleQuestionBase.width
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            horizontalAlignment: Text.AlignJustify
            font.pixelSize: 20
        }
    }

    Component.onCompleted: {

    }
}
