import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "#04A7FF"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Лекция 3</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Команды DML</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\t<b>Data Manipulation Language (DML)</b> (язык управления (манипулирования) данными) — это семейство компьютерных языков, используемых в компьютерных программах или пользователями баз данных для получения, вставки, удаления или изменения данных в базах данных.<br>\tНа текущий момент наиболее популярным языком DML является SQL, используемый для получения и манипулирования данными в РСУБД. Другие формы DML использованы в IMS/DL1, базах данных CODASYL (таких как IDMS), и других.\t<br>Языки DML изначально использовались только компьютерными программами, но с появлением SQL стали также использоваться и людьми.<br>\tФункции языков DML определяются первым словом в предложении (часто называемом запросом), которое почти всегда является глаголом. В случае с SQL эти глаголы — <b>«select»</b> («выбрать»), <b>«insert»</b> («вставить»), <b>«update»</b> («обновить»), и <b>«delete»</b> («удалить»). Это превращает природу языка в ряд обязательных утверждений (команд) к базе данных.\t<br>Языки DML могут существенно различаться у различных производителей СУБД. Существует стандарт SQL, установленный ANSI, но производители СУБД часто предлагают свои собственные «расширения» языка."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<b>Инструкция INSERT</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\tДобавляет одну или несколько строк в таблицу или представление"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>[ WITH common_table_expression [ ,...n ] ]<br>INSERT<br>{<br>\t\t[ TOP ( expression ) [ PERCENT ] ] <br>\t\t[ INTO ]<br>\t\t{ object | rowset_function_limited<br>\t\t[ WITH ( Table_Hint_Limited [ ...n ] ) ]<br>}<br>\t{<br>\t\t[ ( column_list ) ]<br>\t\t[ OUTPUT Clause ]<br>\t\t{ VALUES ( { DEFAULT | NULL | expression } [ ,...n ] ) [ ,...n     ]<br>\t\t| derived_table <br>\t\t| execute_statement<br>\t\t| dml_table_source<br>\t\t| DEFAULT VALUES <br>\t}<br>}<br>[;]</b><br>"
                    }

                    Text{
                        text: "\t<b>WITH common_table_expression</b> - Определяет временный именованный результирующий набор, также называемый обобщенным табличным выражением, определенным в области инструкции <b>INSERT</b>. Результирующий набор получается из инструкции <b>SELECT</b>. Дополнительные сведения см. в разделе <b>WITH</b> обобщенное_табличное_выражение.<br>\t<b>TOP (expression) [ PERCENT ]</b> - Задает число или процент вставляемых случайных строк. expression может быть либо числом, либо процентом от числа строк. Дополнительные сведения см. в разделе TOP.<br>\t<b>INTO</b> - Необязательное ключевое слово, которое можно использовать между ключевым словом <b>INSERT</b> и целевой таблицей.<br>\t<b>server_name</b> - Имя связанного сервера, на котором расположены таблица или представление. server_name может указываться как имя связанного сервера или с помощью функции OPENDATASOURCE.<br>\tЕсли аргумент server_name задается в виде связанного сервера, то необходимы аргументы database_name и schema_name. Если аргумент server_name задается с помощью функции OPENDATASOURCE, то аргументы database_name и schema_name могут применяться не ко всем источникам данных, в зависимости от возможностей поставщика OLE DB, который обращается к удаленному объекту.<br>\t<b>database_name</b> - Имя базы данных.<br>\t<b>schema_name</b> - Имя схемы, к которой принадлежит таблица или представление.<br>\t<b>table_or view_name</b> - Имя таблицы или представления, которые принимают данные.<br>\tВ качестве источника таблицы в инструкции <b>INSERT</b> можно использовать табличную переменную внутри своей области.<br>\tПредставление, на которое ссылается аргумент <b>table_or_view_name</b>, должно быть обновляемым и ссылаться только на одну базовую таблицу в предложении <b>FROM</b> данного представления. Например, инструкция <b>INSERT</b> в многотабличном представлении должна использовать аргумент <b>column_list</b>, который ссылается только на столбцы из одной базовой таблицы. Дополнительные сведения об обновляемых представлениях.<br>\t<b>WITH ( table_hint_limited [... n ] )</b> - Задает одно или несколько табличных указаний, разрешенных для целевой таблицы. Необходимо использовать ключевое слово WITH и круглые скобки.<br>\t<b>(column_list)</b> - Список, состоящий из одного или нескольких столбцов, в которые вставляются данные. Список <b>column_list</b> должен быть заключен в круглые скобки, а его элементы должны разделяться запятыми.<br>\t<b>VALUES</b> - Позволяет использовать один или несколько списков вставляемых значений данных. Для каждого столбца в column_list, если этот параметр указан или присутствует в таблице, должно быть одно значение. Список значений должен быть заключен в скобки.<br>\t<b>DEFAULT</b> - Указывает компоненту Database Engine необходимость принудительно загружать значения по умолчанию, определенные для столбца. Если для столбца не задано значение по умолчанию и он может содержать значение NULL, вставляется значение NULL. В столбцы с типом данных timestamp вставляется следующее значение отметки времени. Значение DEFAULT недопустимо для столбца идентификаторов.<br>\t<b>expression</b> - Константа, переменная или выражение. Выражение не может содержать инструкцию EXECUTE.<br>\t<b>WHERE search_condition</b> - Любое предложение WHERE, содержащее допустимый критерий поиска <search_condition>, фильтрующее строки, которые возвращены аргументом dml_statement_with_output_clause. Дополнительные сведения см. в разделах Условие поиска. При использовании в этом контексте критерий search_condition не должен содержать вложенных запросов, определяемых пользователем скалярных функций, выполняющих доступ к данным, агрегатных функций, TEXTPTR или полнотекстовых предикатов поиска."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<b>Инструкция UPDATE</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\t<b>UPDATE</b> — оператор языка SQL, позволяющий обновить значения в заданных столбцах таблицы.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>[ WITH common_table_expression [...n] ]<br>UPDATE<br>\t[ TOP ( expression ) [ PERCENT ] ]{ <object> | rowset_function_limited <br>\t[ WITH ( <Table_Hint_Limited> [ ...n ] ) ]<br>\t}<br>\tSET<br>\t    <br>\t{ column_name = { expression | DEFAULT | NULL }<br>\t| { udt_column_name.{ { property_name = expression <br>\t| field_name = expression } <br>\t| method_name ( argument [ ,...n ] ) <br>\t}<br>\t}<br>\t\t| column_name { .WRITE ( expression , @Offset , @Length ) }<br>\t\t| @variable = expression <br>\t\t| @variable = column = expression [ ,...n ]<br>\t\t} [ ,...n ]<br>\t[ <OUTPUT Clause> ]<br>\t[ FROM{ <table_source> } [ ,...n ] ] <br>\t[ WHERE { <search_condition> | { [ CURRENT OF { { [ GLOBAL ] cursor_name } | cursor_variable_name }]}]<br>\t[ OPTION ( <query_hint> [ ,...n ] ) ]<br>[ ; ]</b>"
                    }

                    Text{
                        text: "<br>\t<b>WITH common_table_expression</b> - Задает временный именованный результирующий набор или представление, которые называются обобщенным табличным выражением (CTE), определяемым в пределах области действия инструкции UPDATE. Результирующий набор CTE, на который ссылается инструкция UPDATE, является производным простого запроса.<br>\t<b>server_name</b> - Имя сервера (с использованием имени связанного сервера или функции OPENDATASOURCE в качестве имени сервера), на котором расположена таблица или представление. Если задан аргумент server_name, то необходимо также, чтобы были указаны аргументы database_name и schema_name.<br>\t<b>database_name</b> - Имя базы данных.<br>\t<b>schema_name</b> - Имя схемы, к которой принадлежит таблица или представление.<br>\t<b>table_or view_name</b> - Имя таблицы или представления, из которых должны обновляться строки.<br>\t<b>rowset_function_limited</b> - Функция OPENQUERY или OPENROWSET, в зависимости от возможностей поставщика. Дополнительные сведения о возможностях, требующихся поставщику, см. в разделе Требования UPDATE и DELETE для поставщиков OLE DB.<br>\t<b>WITH ( Table_Hint_Limited)</b> - Задает одну или несколько табличных подсказок, разрешенных для целевой таблицы. Ключевое слово WITH и круглые скобки обязательны. Использование аргументов NOLOCK и READUNCOMMITTED запрещено. Сведения о табличных подсказках.<br>\t<b>SET</b> - Задает список обновляемых имен столбцов или переменных.<br>\t<b>column_name</b> - Столбец, содержащий обновляемые данные. Столбец с именем column_name должен существовать в <b>table_or view_name</b>. Столбцы идентификаторов не могут быть обновлены.<br>\t<b>expression</b> - Переменная, символьное значение, выражение или подзапрос выборки (заключенный в скобки), которые возвращают единственное значение. Значение, возвращаемое expression, заменяет существующее значение в column_name или @variable.<br>\t<b>DEFAULT</b> - Определяет, что существующее значение в столбце должно заменяться значением по умолчанию. Также может использоваться для присвоения значения NULL, если столбец не имеет значений по умолчанию и может принимать значения NULL.<br>\t<b>udt_column_name</b> - Столбец пользовательского типа.<br>\t<b>property_name | field_name</b> - Общедоступное свойство или общедоступный элемент данных пользовательского типа.<br>\t<b>method_name(argument [ ,... n] )</b> - Не статичный метод общего мутатора udt_column_name, принимающий один или несколько аргументов.<br>\t<b>.WRITE expression, @Offset, @Length)</b> - Указывает, что должен быть изменен раздел значения column_name. expression заменяет в column_name@Length элементы, начиная с позиции @Offset. С этим предложением можно указывать только столбцы типа varchar(max), nvarchar(max) или varbinary(max). column_name не может иметь значения NULL и не может дополняться именем таблицы или псевдонимом таблицы.<br>\t<b>expression</b> — значение, которое копируется в столбец column_name. Аргумент expression должен иметь результат типа column_name или неявно приводиться к этому типу. Если для expression установлено значение NULL, аргумент @Length не учитывается, а значение в column_name усекается с позиции, на которую указывает аргумент @Offset.<br>\t<b>SET @variable = column = expression</b> -  присваивает переменной то же значение, что и столбцу. Это отличается от предложения SET @variable = column, column = expression, присваивающего переменной значение столбца до обновления.<br>\t<b>OUTPUT_Clause</b> - Возвращает обновленные данные или основанные на них выражения в рамках выполнения операции UPDATE. Предложение OUTPUT не поддерживается ни в одной инструкции DML, целью которой являются удаленные таблицы или представления. Дополнительные сведения.<br>\t<b>FROM источник_таблицы</b> - Определяет, что для определения критериев операции обновления используется таблица, представление или производный источник таблицы. Дополнительные сведения.<br>\t<b>WHERE</b> - Задает условия, ограничивающие обновляемые строки. Существует два вида обновлений, в зависимости от используемой формы предложения WHERE.<br>\t<b>CURRENT OF</b> - Определяет, что обновление выполняется в текущей позиции указанного курсора.<br>\t<b>GLOBAL</b> - Указывает, что аргумент cursor_name ссылается на глобальный курсор."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<b>Инструкция DELETE</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    CodeExample{
                        exampleText: "<b>[ WITH <common_table_expression> [ ,...n ] ]<br>DELETE<br>\t[ TOP ( expression ) [ PERCENT ] ] [ FROM ] { <object> | rowset_function_limited <br>\t\t[ WITH ( <table_hint_limited> [ ...n ] ) ]<br>\t}<br>\t[ <OUTPUT Clause> ][ FROM <table_source> [ ,...n ] ][ WHERE { <search_condition> | { [ CURRENT OF<br>\t\t{ { [ GLOBAL ] cursor_name } | cursor_variable_name }]}}]<br>\t[ OPTION ( <Query Hint> [ ,...n ] ) ] <br>[; ]<br>"
                    }

                    Text{
                        text: "\t<b>WITH common_table_expression</b> - Задает временный именованный результирующий набор, также называемый обобщенным табличным выражением, который определяется в области действия инструкции DELETE. Результирующий набор получается из инструкции SELECT.<br>\t<b>TOP (expression) [ PERCENT ]</b> - Задает число или процент случайных строк для удаления. Выражение expression может быть либо числом, либо процентом строк. Строки, на которые ссылается выражение TOP, используемое с инструкциями INSERT, UPDATE и DELETE, не упорядочиваются.<br>\t<b>FROM</b> - Необязательное ключевое слово, которое можно использовать между ключевым словом DELETE и целевым аргументом table_or_view_name или rowset_function_limited.<br>\t<b>server_name</b>Имя сервера (с использованием имени связанного сервера или функции OPENDATASOURCE в качестве имени сервера), на котором расположена таблица или представление. Если указан аргумент server_name, также необходимо указать аргументы database_name и schema_name.<br>\t<b>database_name</b> - Имя базы данных.<br>\t<b>schema_name</b> - Имя схемы, которой принадлежит таблица или представление.<br>\t<b>table_or view_name</b> - Имя таблицы или представления, откуда удаляются строки.<br>\t<b>rowset_function_limited</b> - Функция OPENQUERY или OPENROWSET в зависимости от возможностей поставщика. Дополнительные сведения о возможностях, требуемых для поставщика см. в разделе Требования UPDATE и DELETE для поставщиков OLE DB.<br>\t<b>WITH ( table_hint_limited [... n] )</b> - Задает одну или несколько табличных подсказок, разрешенных для целевой таблицы. Ключевое слово WITH и круглые скобки обязательны. Использование ключевых слов NOLOCK и READUNCOMMITTED запрещено. Дополнительные сведения о табличных подсказках.<br>\t<b>OUTPUT_Clause</b> - Возвращает удаленные строки или выражения, основанные на них, как часть операции DELETE. Предложение OUTPUT не поддерживается ни в каких инструкциях DML, направленных на представления и удаленные таблицы.<br>\t<b>FROM table_source</b> - Задает дополнительное предложение FROM. Это расширение языка Transact-SQL для инструкции DELETE позволяет задавать данные из table_source и удалять соответствующие строки из таблицы в первом предложении FROM.<br>\t<b>WHERE</b> - Указывает условия, используемые для ограничения числа удаляемых строк. Если предложение WHERE не указывается, инструкция DELETE удаляет все строки из таблицы.<br>\t<b>search_condition</b> - Указывает ограничивающие условия для удаляемых строк. Количество предикатов, которое может содержать условие поиска, не ограничено."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Контрольные вопросы:</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    // question 1
                    LessonSingleQuestion{
                        id: q1
                        question : "Для чего предназначен язык DML?"
                    }

                    SingleQuestionVariant{
                        id: q1v1
                        variantText: "Для изменеия состояния объектов базы данных, создания и удаления объектов и баз данных"
                    }
                    SingleQuestionVariant{
                        id: q1v2
                        variantText: "Для изменения данных, которые записаны в таблицах баз данных и выборки данных."// *
                    }
                    SingleQuestionVariant{
                        id: q1v3
                        variantText: "Для реализации классическиех управляющик конструкций императивных языков программирования в структурированыз языках управления данными."
                    }
                    SingleQuestionVariant{
                        id: q1v4
                        variantText: "Только для изменения данных в таблицах"
                    }

                    // question 2
                    LessonSingleQuestion{
                        id: q2
                        question : "Какие операторы отнясяться к языку DML?"
                    }

                    SingleQuestionVariant{
                        id: q2v1
                        variantText: "VIEW"
                    }

                    SingleQuestionVariant{
                        id: q2v2
                        variantText: "DROP"
                    }
                    SingleQuestionVariant{
                        id: q2v3
                        variantText: "CREATE"
                    }
                    SingleQuestionVariant{
                        id: q2v4
                        variantText: "INSERT" // +
                    }
                    SingleQuestionVariant{
                        id: q2v5
                        variantText: "UPDATE" // +
                    }
                    SingleQuestionVariant{
                        id: q2v6
                        variantText: "DELETE" // +
                    }
                    SingleQuestionVariant{
                        id: q2v7
                        variantText: "SET"
                    }
                    SingleQuestionVariant{
                        id: q2v8
                        variantText: "VERSION"
                    }

                    // question 3
                    LessonSingleQuestion{
                        id: q3
                        question : "Для чего предназначен оператор INSERT?"
                    }

                    SingleQuestionVariant{
                        id: q3v1
                        variantText: "Показывает содержимое таблицы в базе данных."
                    }

                    SingleQuestionVariant{
                        id: q3v2
                        variantText: "Показывает содержимое записи в таблице базы данных"
                    }

                    SingleQuestionVariant{
                        id: q3v3
                        variantText: "Создает новую запись в таблице баз данных." // +
                    }

                    SingleQuestionVariant{
                        id: q3v4
                        variantText: "Изменяет данные записи."
                    }

                    // question 4

                    LessonSingleQuestion{
                        id: q4
                        question : "Для чего предназначен оператор UPDATE?"
                    }

                    SingleQuestionVariant{
                        id: q4v1
                        variantText: "Обновляет содержимое таблицы."
                    }

                    SingleQuestionVariant{
                        id: q4v2
                        variantText: "Обновляет состояние всей базы данных."
                    }

                    SingleQuestionVariant{
                        id: q4v3
                        variantText: "Вносит изменение данных в запись таблицы." // +
                    }

                    // question 5

                    LessonSingleQuestion{
                        id: q5
                        question : "Какая разница между операторами DROP и DELET?"
                    }

                    SingleQuestionVariant{
                        id: q5v1
                        variantText: "Разницы нет. Операторы работают идентично."
                    }
                    SingleQuestionVariant{
                        id: q5v2
                        variantText: "Оператор Drop удаляет записи из таблицы безвозвратно, действия оператора DELETE могут быть отменены."
                    }
                    SingleQuestionVariant{
                        id: q5v3
                        variantText: "Оператор DROP изменяет структуру базы данных, удаляет объекты базы данных. Оператор DELETE удаляет записи из таблицы." // +
                    }


                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_lecture3()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "#04A7FF"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
