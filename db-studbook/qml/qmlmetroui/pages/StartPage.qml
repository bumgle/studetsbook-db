import QtQuick 1.1
import CustomComponents 1.0
import Bridge 1.0
import "pagescripts.js" as PageScripts

Item{

    Database{
        id: db
    }


    id: startBase

    Text{
        id: bookName
        x: 10
        y: 25
        text: qsTr("Базы данных")
        color: "white"
        font.family: "Monaco"
        font.bold: true
        font.pixelSize: 50
    }

    Line{
        id: separaror
        x1: 0
        y1: 100
        x2: root.width - leftMenu.width - 10 * 3
        y2: 100
        color: "white"
        penWidth: 2
    }

    Text{
        id: bookType
        x: 10
        y: 125
        text: qsTr("Электронный учебник")
        color: "white"
        font.family: "Monaco"
        font.bold: true
        font.pixelSize: 30
    }




    Rectangle {
        id: whole
        anchors.left: startBase.left
        anchors.bottom: startBase.bottom
        anchors.bottomMargin: 10

        width: 85
        height: 85
        color: "#CD3278"

        Text{
            id: headerWhole
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Заработано");
            color: "white"
            font.family: "Monaco"
            font.bold: true
        }

        Text{
            id: valueWhole
            anchors.top: headerWhole.bottom
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            text: "*"
            color: "white"
            font.family: "Monaco"
            font.bold: true
            font.pixelSize: 30

        }


    }


    Rectangle {
        id: completed
        anchors.left: whole.right
        anchors.leftMargin: 10
        anchors.bottom: startBase.bottom
        anchors.bottomMargin: 10
        width: 85
        height: 85
        color: "#CD3278"

        Text{
            id: headerCompleted
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Всего");
            color: "white"
            font.family: "Monaco"
            font.bold: true
        }

        Text{
            id: valueCompleted
            anchors.top: headerCompleted.bottom
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            text: "100"
            color: "white"
            font.family: "Monaco"
            font.bold: true
            font.pixelSize: 30

        }
    }

    Rectangle{
        id: labelCompleted
        anchors.left: startBase.left
        anchors.bottom: whole.top
        anchors.bottomMargin: 10
        color: "#CD3278"
        width: 180
        height: 85

        Image{
            id: labelCompletedImg
            source: "qrc:/pics/pictures/favs.png"
            anchors.left: labelCompleted.left
            anchors.leftMargin: 3
            anchors.verticalCenter: labelCompleted.verticalCenter
        }

        Text{
            anchors.left: labelCompletedImg.right
            anchors.leftMargin: 10
            anchors.verticalCenter: labelCompleted.verticalCenter
            text: qsTr("Статистика <br> успеваемости");
            color: "white"
            font.pixelSize: 15
            font.bold: true
        }
    }

    Component.onCompleted: {
        db.initDatabase()
        PageScripts.refreshStatsMainPage()
    }
}
