import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "red"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Практика 1</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Создание удаление и модификация объектов баз данных</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    // exicise 1
                    Text{
                        text: "\tСоздайте базу данных под именем <b>car_shop</b>."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 40
                    }

                    Rectangle{
                        id: query_1_base
                        color: "#bfddf3"
                        width: pageContent.width - 50
                        height: 50

                        TextInput{
                            id: query_1_text
                            anchors.fill: parent
                            anchors.leftMargin: 20
                            anchors.rightMargin: 20
                            horizontalAlignment: TextInput.AlignLeft
                            font.pixelSize: 40
                            font.bold: true
                            color: "black"
                        }
                     }

                        // exicise 2
                        Text{
                            text: "\tИспользуя команду <b>use</b> перейдите к базе даных <b>car_shop</b>"
                            color: "black"
                            width: pageContent.width - 20
                            wrapMode: Text.WordWrap
                            textFormat: Text.StyledText
                            horizontalAlignment: Text.AlignJustify
                            font.pixelSize: 40
                        }

                        Rectangle{
                            id: query_2_base
                            color: "#bfddf3"
                            width: pageContent.width - 50
                            height: 50

                            TextInput{
                                id: query_2_text
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                horizontalAlignment: TextInput.AlignLeft
                                font.pixelSize: 40
                                font.bold: true
                                color: "black"
                            }

                    }

                    // exicise 3
                        Text{
                            text: "\tИспользуя команду <b>show tables</b> просмотрите список таблиц, которые содержаться в базе данных car_shop</b>"
                            color: "black"
                            width: pageContent.width - 20
                            wrapMode: Text.WordWrap
                            textFormat: Text.StyledText
                            horizontalAlignment: Text.AlignJustify
                            font.pixelSize: 40
                        }

                        Rectangle{
                            id: query_3_base
                            color: "#bfddf3"
                            width: pageContent.width - 50
                            height: 50

                            TextInput{
                                id: query_3_text
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                horizontalAlignment: TextInput.AlignLeft
                                font.pixelSize: 40
                                font.bold: true
                                color: "black"
                            }

                    }


                    // exicise 4
                        Text{
                            text: "\tСоздайте таблицу <b>cars</b> которая содержит поля <b>id</b> (целочисленое ,первичный ключ, не нулевое значение), <b>maker</b> (массив символов, размер 20), <b>model</b> (массив символов, размер 20), <b>year</b>(целочисленное), <b>color</b> (массив символов, размер 20)</b>"
                            color: "black"
                            width: pageContent.width - 20
                            wrapMode: Text.WordWrap
                            textFormat: Text.StyledText
                            horizontalAlignment: Text.AlignJustify
                            font.pixelSize: 40
                        }

                        Rectangle{
                            id: query_4_base
                            color: "#bfddf3"
                            width: pageContent.width - 50
                            height: 50

                            TextInput{
                                id: query_4_text
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                horizontalAlignment: TextInput.AlignLeft
                                font.pixelSize: 40
                                font.bold: true
                                color: "black"
                            }

                    }


                    // exicise 5
                        Text{
                            text: "\tИспользуя команду <b>describe</b> выведите параметры таблицы <b>cars</b></b>"
                            color: "black"
                            width: pageContent.width - 20
                            wrapMode: Text.WordWrap
                            textFormat: Text.StyledText
                            horizontalAlignment: Text.AlignJustify
                            font.pixelSize: 40
                        }

                        Rectangle{
                            id: query_5_base
                            color: "#bfddf3"
                            width: pageContent.width - 50
                            height: 50

                            TextInput{
                                id: query_5_text
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                horizontalAlignment: TextInput.AlignLeft
                                font.pixelSize: 40
                                font.bold: true
                                color: "black"
                            }

                    }
                        // exercise 6

                        Text{
                            text: "\tУдалите таблицу <b>cars</b></b>"
                            color: "black"
                            width: pageContent.width - 20
                            wrapMode: Text.WordWrap
                            textFormat: Text.StyledText
                            horizontalAlignment: Text.AlignJustify
                            font.pixelSize: 40
                        }

                        Rectangle{
                            id: query_6_base
                            color: "#bfddf3"
                            width: pageContent.width - 50
                            height: 50

                            TextInput{
                                id: query_6_text
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                horizontalAlignment: TextInput.AlignLeft
                                font.pixelSize: 40
                                font.bold: true
                                color: "black"
                            }

                    }

                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_practice1()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "red"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
