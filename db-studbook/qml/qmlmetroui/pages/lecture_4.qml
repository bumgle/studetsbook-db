import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "#04A7FF"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Лекция 4</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Оператор выборки SELECT</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\t<b>SELECT</b> (англ., означает «выбрать») — оператор DML языка SQL, возвращающий набор данных (выборку) из базы данных, удовлетворяющих заданному условию.<br>\tВ большинстве случаев, выборка осуществляется из одной или нескольких таблиц. В последнем случае говорят об операции слияния (JOIN (SQL)). В тех СУБД, где реализованы представления (англ. view) и хранимые процедуры (англ. stored procedure), также возможно получение соответствующих наборов данных.<br>\tПри формировании запроса SELECT пользователь описывает ожидаемый набор данных: его вид (набор столбцов) и его содержимое (критерий попадания записи в набор, группировка значений, порядок вывода записей и т. п.).<br>\Запрос выполняется следующим образом: сначала извлекаются все записи из таблицы, а затем для каждой записи набора проверяется её соответствие заданному критерию. Если осуществляется слияние из нескольких таблиц, то сначала составляется произведение таблиц, а уже затем из полученного набора отбираются требуемые записи.<br>\tОсобую роль играет обработка NULL-значений, когда при слиянии, например, двух таблиц — главной (англ. master) и подчинённой (англ.detail) — имеются или отсутствуют соответствия между записями таблиц, участвующих в слиянии. Для решения этой задачи используются механизмы внутреннего (англ. inner) и внешнего (англ. outer) слияния.<br>\tОдин и тот же набор данных может быть получен при выполнении различных запросов. Поиск оптимального плана выполнения данного запроса является задачей оптимизатора.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<br>\t<b>Структура оператора<br></b><br>\tОператор SELECT имеет следующую структуру:<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT <br>\t[DISTINCT | DISTINCTROW | ALL]<br>\tselect_expression,...<br>\t[FROM table_references]<br>\t[WHERE where_definition]<br>\t[GROUP BY {unsigned_integer | col_name | formula}]<br>\t[HAVING where_definition]<br>\t[ORDER BY {unsigned_integer | col_name | formula} [ASC | DESC], ...]</b>"
                    }

                    Text{
                        text: "<br>\tОсновные ключевые слова, относящиеся к запросу <b>SELECT</b>:<br>\t<b>WHERE</b> — используется для определения, какие строки должны быть выбраны или включены в <b>GROUP BY</b>.<br>\t<b>GROUP BY</b> — используется для объединения строк с общими значениями в элементы меньшего набора строк.<br>\t<b>HAVING</b> — используется для определения, какие строки после <b>GROUP BY</b> должны быть выбраны.<br>\t<b>ORDER BY</b> — используется для определения, какие столбцы используются для сортировки результирующего набора данных.<br>\tДля таблицы T запрос<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT</b> список полей <b>FROM</b> список таблиц <b>WHERE условия…</b>"
                    }

                    Text{
                        text: "\tОсновные ключевые слова, относящиеся к запросу <b>SELECT</b>:<br>\t<b>WHERE</b> — используется для определения, какие строки должны быть выбраны или включены в <b>GROUP BY</b>.<br>\t<b>GROUP BY</b> — используется для объединения строк с общими значениями в элементы меньшего набора строк.<br>\t<b>HAVING</b> — используется для определения, какие строки после <b>GROUP BY</b> должны быть выбраны.<br>\t<b>ORDER BY</b> — используется для определения, какие столбцы используются для сортировки результирующего набора данных.<br>\tДля таблицы T запрос<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM T;</b>"
                    }

                    Text{
                        text: "вернёт все столбцы всех строк данной таблицы. Для той же таблицы запрос"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT C1 FROM T;</b>"
                    }

                    Text{
                        text: "вернёт значения столбца C1 всех строк таблицы— в терминахреляционной алгебры можно сказать, что была выполнена проекция. Для той же таблицы запрос"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM T WHERE C1 = 1;</b>"
                    }

                    Text{
                        text: "вернёт значения всех столбцов всех строк таблицы, у которых значение поля C1 равно '1'— в терминах реляционной алгебры можно сказать, что была выполнена выборка, так как присутствует ключевое слово <b>WHERE</b>. Запрос"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM T ORDER BY C1 DESC;</b>"
                    }

                    Text{
                        text: "вернёт те же строки, что и первый, однако результат будет отсортирован в обратном порядке <b>(Z-A)</b> из-за использования ключевого слова <b>ORDER BY</b> с полем C1 в качестве поля сортировки. Этот запрос не содержит ключевого слова <b>WHERE</b>, поэтому он вернёт всё, что есть в таблице. Несколько элементов <b>ORDER BY</b> могут быть указаны разделённые запятыми [напр. ORDER BY C1 ASC, C2 DESC] для более точной сортировки.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }



                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Контрольные вопросы:</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    // question 1
                    LessonSingleQuestion{
                        id: q1
                        question : "Охарактеризируйте операратор SELECT?"
                    }

                    SingleQuestionVariant{
                        id: q1v1
                        variantText: "Данный оператор предназначен для произведения выборок информации из таблицы баз данных, дает широкие возможности для сортировки и выборки данных." // *
                    }
                    SingleQuestionVariant{
                        id: q1v2
                        variantText: "Создает представление информации извлеченной из базы данных."
                    }
                    SingleQuestionVariant{
                        id: q1v3
                        variantText: "Производит выбор объекта баз данных с котором будут производиться дальнейшие операции."
                    }

                    // question 2
                    LessonSingleQuestion{
                        id: q2
                        question : "Для чего предназначен модификатор WHERE?"
                    }

                    SingleQuestionVariant{
                        id: q2v1
                        variantText: "Задаеться локация в базы данных в которой будет осуществляться поиск."
                    }
                    SingleQuestionVariant{
                        id: q2v2
                        variantText: "Задаються параметры, котором должны отвечать данные, которые храняться в определенных колонках таблицы для соответствия параметрам выборки." // +
                    }
                    SingleQuestionVariant{
                        id: q2v3
                        variantText: "Производит выбор объекта баз данных с котором будут производиться дальнейшие операции."
                    }

                    // question 3
                    LessonSingleQuestion{
                        id: q3
                        question : "Какие функции выполняет оператор GROUPE BY?"
                    }

                    SingleQuestionVariant{
                        id: q3v1
                        variantText: "Групирует значения колонок выборки." // +
                    }
                    SingleQuestionVariant{
                        id: q3v2
                        variantText: "Групирует значения строк выборки."
                    }
                    SingleQuestionVariant{
                        id: q3v3
                        variantText: "Сортирует строки выборки в восходящем порядке."
                    }

                    // question 4

                    LessonSingleQuestion{
                        id: q4
                        question : "Какие функции выполняет оператор ORDER BY заданный без параметров?"
                    }

                    SingleQuestionVariant{
                        id: q4v1
                        variantText: "Групирует значения колонок выборки."
                    }
                    SingleQuestionVariant{
                        id: q4v2
                        variantText: "Сортирует строки выборки в нисходящем порядке."
                    }
                    SingleQuestionVariant{
                        id: q4v3
                        variantText: "Сортирует строки выборки в восходящем порядке." // +
                    }

                    // question 5

                    LessonSingleQuestion{
                        id: q5
                        question : "Какую выборку производит данный запрос: SELECT * FROM car WHERE year > 2000 ORGER BY DESC?"
                    }

                    SingleQuestionVariant{
                        id: q5v1
                        variantText: "Из таблицы car производиться выборка записей, поле year в которых имеет значение болеше 2000, данные сортируються в нисходящем порядке." // +
                    }
                    SingleQuestionVariant{
                        id: q5v2
                        variantText: "Из таблицы car производиться выборка записей, поле year в которых имеет значение болеше 2000, сортировка данных отменяються."
                    }
                    SingleQuestionVariant{
                        id: q5v3
                        variantText: "Из таблицы car производиться выборка содержимого одного столбца year, в которых имеет значение болеше 2000, сортировка данных отменяються."
                    }


                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_lecture4()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "#04A7FF"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
