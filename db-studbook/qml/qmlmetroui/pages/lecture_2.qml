import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "#04A7FF"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Лекция 2</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Язык описания данных DDL.</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\t<b>Data Definition Language (DDL) (язык описания данных)</b> — это язык программирования который используеться для описания структур данных и объектов баз данных.<br>\tDDL является составляющей частью языка управления базами данных SQL, который используеться для получения и манипулирования данными в РСУБД, и сочетает в себе элементы DDL, DML и DCL.<br>\tФункции языков DDL определяются первым словом в предложении (часто называемом запросом), которое почти всегда является глаголом. В случае с SQL эти глаголы — <b>«create»</b> («создать»), <b>«alter»</b> («изменить»), <b>«drop»</b> («удалить»). Это превращает природу языка в ряд обязательных утверждений (команд) к базе данных. Языки DDL могут существенно различаться у различных производителей СУБД. Существует ряд стандартов SQL, установленный ISO/IEC (SQL-89,SQL-92, SQL:1999, SQL:2003, SQL:2008), но производители СУБД часто предлагают свои собственные «расширения» языка и, часто, не поддерживают стандарт полностью.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<b>Команда CREATE</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\t<b>CREATE</b> — DDL оператор языка SQL, используемый для создания объектов базы данных. Различные СУБД работают с различными объектами.<br>\tСтандарт SQL-92 определяет команду CREATE в вариантах:<br>\t<b>CREATE ASSERTION</b> — создание утверждения<br>\t<b>CREATE CHARACTER SET</b> — создание набора символов<br>\t<b>CREATE COLLATION</b> — создание правила сортировки для набора символов<br>\t<b>CREATE DOMAIN</b> — создание домена (пользовательского типа данных столбца)<br>\t<b>CREATE SCHEMA</b> — создание схемы (именованной группы объектов)<br>\t<b>CREATE TABLE</b> — создание таблицы базы данных<br>\t<b>CREATE TRANSLATION</b> — создание правила преобразования (трансляции) из одного набора символов в другой (используется в операторе TRANSLATE)<br>\t<b>CREATE VIEW</b> — создание представления данных<br>/tНаиболее общие команды (поддерживаются большинством СУБД): <b>CREATE TABLE и CREATE VIEW</b><br>\tПриведем упрощенный синтаксис SQL-предложения для создания таблицы на SQL-сервере InterBase (более полный синтаксис можно посмотреть в online-справочнике по SQL, поставляемом с локальным InterBase):<br><br>\t<b>CREATE TABLE table<br>\t(col_def [, col_def | tconstraint ...]);</b><br>\tгдe<br>\t<b>table</b> - имя создаваемой таблицы,<br>\t<b>col_def</b> - описание поля,<br>\t<b>tconstraint</b> - описание ограничений и/или ключей (квадратные скобки [] означают необязательность, вертикальная черта | означает или).<br><br>\tОписание поля состоит из наименования поля и типа поля, а также дополнительных ограничений, накладываемых на поле:<br>\t<b>col_def = col {datatype | COMPUTED BY (expr) | domain}</b><br>\t<b>[DEFAULT {literal | NULL | USER}]</b><br>\t<b>[NOT NULL] [col_constraint]</b><br>\t<b>[COLLATE collation]</b><br><br>\tЗдесь:<br>\t<b>col</b> - имя поля;<br>\t<b>datatype</b> - любой правильный тип SQL-сервера (для InterBase такими типами являются - <b>SMALLINT, INTEGER, FLOAT, DOUBLE PRECISION, DECIMAL, NUMERIC, DATE, CHAR, VARCHAR, NCHAR, BLOB</b>), символьные типы могут иметь <b>CHARACTER SET</b> - набор символов, определяющий язык страны. Для русского языка следует задать набор символов WIN1251;<br>\t<b>COMPUTED BY (<expr>)</b></br> - определение вычисляемого на уровне сервера поля, где expr - правильное SQL-выражение, возвращающее единственное значение;<br>" +
"\t<b>domain</b> - имя домена (обобщенного типа), определенного в базе данных;<br>\t<b>DEFAULT</b> - конструкция, определяющая значение поля по умолчанию;<br>\t<b>NOT NULL</b> - конструкция, указывающая на то, что поле не может быть пустым;<br>\t<b>COLLATE</b> - предложение, определяющее порядок сортировки для выбранного набора символов (для поля типа BLOB не применяется). Русский набор символов WIN1251 имеет 2 порядка сортировки - WIN1251 и PXW_CYRL. Для правильной сортировки, включающей большие буквы, следует выбрать порядок <b>PXW_CYRL</b>.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }


                    Text{
                        text: "Пример создания таблицы:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{

                        exampleText: "<b>CREATE TABLE cars(id INT PRIMARY KEY, vin INT NOT NULL, car_model VARCHAR(20) NOT NULL);</b>"
                    }


                     Text{
                        text: "<br><b>Команда ALTER</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                     }


                    Text{
                        text: "\tКоманда <b>ALTER TABLE</b> позволяет изменить структуру таблицы. Эта команда позволяет добавлять и удалять столбцы, создавать и уничтожать индексы, переименовывать столбцы и саму таблицу. Команда имеет следующий синтаксис:<br>\t<b>ALTER TABLE table_name alter_spec</b><br>Параметр alter_spec имеет значения:<br><b>\tADD create_definition [FIRST|AFTER column_name]</b> - Добавление нового столбца create_definition. create_definition представляет собой название нового столбца и его тип. Конструкция FIRST добавляет новый столбец перед столбцом column_name. Конструкция AFTER добавляет новый столбец после столбца column_name. Если место добавления не указано, по умолчанию столбец добавляется в конец таблицы.<br><b>\tADD INDEX [index_name] (index_col_name,...)</b> - Добавление индекса index_name для столбца index_col_name. Если имя индекса index_name не указывается, ему присваивается имя совпадающее с именем столбца index_col_name.<br><b>\tADD PRIMARY KEY (index_col_name,...)</b> - Делает столбец index_col_name или группу столбцов первичным ключом таблицы.<br><b>\tCHANGE old_col_name new_col_name type</b> - Изменение столбца с именем old_col_name на столбец с именем new_col_name и типом type.<br><b>\tDROP col_name</b> - Удаление столбца с именем col_name.<br><b>\tDROP PRIMARY KEY</b> - Удаление первичного ключа таблицы.<br><b>\tDROP INDEX index_name</b> - Удаление индекса index_name.<br><br>Добавим в таблицу forums новый столбец test, разместив его после столбца name."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{

                        exampleText: "<b>ALTER TABLE forums ADD test int(10) AFTER name;</b>"
                    }


                    Text{
                       text: "<br><b>Команда DROP</b><br>"
                       font.pixelSize: 20
                       horizontalAlignment: Text.Center
                       textFormat: Text.StyledText
                       color: "black"
                       width: pageContent.width - 20
                       wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\tУдаляет одно или больше определений таблиц и все данные, индексы, триггеры, ограничения и разрешения для этих таблиц. Любое представление или хранимая процедура, ссылающаяся на удаленную таблицу, должна быть явно удалена с помощью инструкции <b>DROP VIEW</b> или <b>DROP PROCEDURE</b>."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{

                        exampleText: "<b>DROP TABLE [ database_name . [ schema_name ] . | schema_name . ]<br>\ttable_name [ ,...n ] [ ; ]</b><br><br>\t<b>database_name</b> - имя базы данных, в которой создана таблица.<br>\t<b>schema_name</b> - имя схемы, которой принадлежит таблица.<br>\t<b>table_name</b> - имя таблицы, предназначенной для удаления."
                    }

                    Text{
                        text: ""
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "\tИнструкцию <b>DROP TABLE</b> нельзя использовать для удаления таблицы, на которую ссылается ограничение <b>FOREIGN KEY</b>. Сначала следует удалить ссылающееся ограничение <b>FOREIGN KEY</b> или ссылающуюся таблицу. Если и ссылающаяся таблица, и таблица, содержащая первичный ключ, удаляются с помощью одной инструкции <b>DROP TABLE</b>, ссылающаяся таблица должна быть первой в списке.<br>\tНесколько таблиц можно удалить из любой базы данных. Если удаляемая таблица ссылается на первичный ключ другой таблицы, которая также удаляется, ссылающаяся таблица с внешним ключом должна стоять в списке перед таблицей, содержащей указанный первичный ключ.<br>\tПри удалении таблицы относящиеся к ней правила и значения по умолчанию теряют привязку, а любые связанные с таблицей ограничения или триггеры автоматически удаляются. Если таблица будет создана заново, нужно будет заново привязать все правила и значения по умолчанию, заново создать триггеры и добавить необходимые ограничения.<br>\tПри удалении всех строк в таблице с помощью инструкции <b>DELETE tablename</b> или <b>TRUNCATE TABLE</b> таблица продолжает существовать, пока она не будет удалена.<br>\tБольшие таблицы и индексы из более чем 128 экстентов удаляются в два этапа: логический и физический. На логическом этапе существующие единицы распределения, используемые в таблице, отмечаются для освобождения и остаются заблокированными до фиксации транзакции. На физическом этапе страницы IAM, отмеченные для освобождения, физически удаляются пакетами.<br>\tПри удалении таблицы, которая содержит столбец <b>VARBINARY(MAX)</b> с атрибутом <b>FILESTREAM</b>, не будут удалены никакие данные, которые хранятся в файловой системе."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "\tСледующий пример удаляет таблицу <b>ProductVendor1</b>, ее данные и индексы из текущей базы данных."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{

                        exampleText: "<b>DROP TABLE ProductVendor1;</b>"
                    }

                    Text{
                        text: "<br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }


                    Text{
                        text: "<b>Контрольные вопросы:</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    // question 1
                    LessonSingleQuestion{
                        id: q1
                        question : "Какое предназначение языка описания данных?"
                    }

                    SingleQuestionVariant{
                        id: q1v1
                        variantText: "Управление информацией."
                    }

                    SingleQuestionVariant{
                        id: q1v2
                        variantText: "Систематизированние и структурированние совокупности знаний."
                    }

                    SingleQuestionVariant{
                        id: q1v3
                        variantText: "Описание струтур данных, объектов баз данных и управление объектами баз данных." //+
                    }

                    SingleQuestionVariant{
                        id: q1v4
                        variantText: "Представление данных пользователю."
                    }

                    //question 2
                    LessonSingleQuestion{
                        id: q2
                        question : "Для чего предназначена команда CEREATE?"
                    }

                    SingleQuestionVariant{
                        id: q2v1
                        variantText: "Для создания записей в таблице."
                    }

                    SingleQuestionVariant{
                        id: q2v2
                        variantText: "Для создания баз данных и объектов баз данных." //+
                    }

                    SingleQuestionVariant{
                        id: q2v3
                        variantText: "Только для создания таблиц."
                    }


                    //question 3
                    LessonSingleQuestion{
                        id: q3
                        question : "Какие дейсивмя с таблицей может выполнить команда ALTER?"
                    }

                    SingleQuestionVariant{
                        id: q3v1
                        variantText: "Вставка новой колонки." //+
                    }

                    SingleQuestionVariant{
                        id: q3v2
                        variantText: "Удаление колонки." //+
                    }

                    SingleQuestionVariant{
                        id: q3v3
                        variantText: "Удаление таблицы."
                    }

                    SingleQuestionVariant{
                        id: q3v4
                        variantText: "Изменение имени таблицы."
                    }
                    SingleQuestionVariant{
                        id: q3v5
                        variantText: "Перемещение таблицы в другую базу данных."
                    }

                    //question 4
                    LessonSingleQuestion{
                        id: q4
                        question : "Для чего предназначена команда DROP?"
                    }

                    SingleQuestionVariant{
                        id: q4v1
                        variantText: "Для удаления баз данных и объектов баз данных." //+
                    }

                    SingleQuestionVariant{
                        id: q4v2
                        variantText: "Для понижения порядка таблиц."
                    }

                    SingleQuestionVariant{
                        id: q4v3
                        variantText: "Для удаления записей из таблицы."
                    }

                    //question 5
                    LessonSingleQuestion{
                        id: q5
                        question : "Что может препятствовать удалению таблицы?"
                    }

                    SingleQuestionVariant{
                        id: q5v1
                        variantText: "Записи содержащиеся в таблице."
                    }

                    SingleQuestionVariant{
                        id: q5v2
                        variantText: "Вторичнык ключи которые связывают удаляемую таблицу с другими объектами базы данных." //+
                    }

                    SingleQuestionVariant{
                        id: q5v3
                        variantText: "Представления и хранимые процедуры, которые связаны с удаляемой таблицей."
                    }

                    SingleQuestionVariant{
                        id: q5v4
                        variantText: "Тригеры, которые связаны с удаляемой таблицей."
                    }

                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_lecture2()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "#04A7FF"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
