function refreshLectureModel() {
    lectures.clear();
    var lcount = db.getLecturesCount()
    var i;
    for (i = 1; i <= lcount; i++){
        var lect = db.getLectureAtributes(i);
        lectures.append({"head": lect[1], "body": lect[2], "ball": lect[3], "v": lect[4], "s": lect[5]});
    }
}

function refreshPracticeModel() {

    lectures.clear();
    var lcount = db.getPracticesCount()
    var i;
    for (i = 1; i <= lcount; i++){
        var lect = db.getPracticeAtributes(i);
        lectures.append({"head": lect[1], "body": lect[2], "ball": lect[3] / 2, "v": lect[4], "s": lect[5]});
    }
}

function refreshSelfJobModel() {

    lectures.clear();
    var lcount = db.getSelfJobCount()
    var i;
    for (i = 1; i <= lcount; i++){
        var lect = db.getSelfJobAtributes(i);
        lectures.append({"head": lect[1], "body": lect[2], "ball": lect[3] / 3, "v": lect[4], "s": lect[5]});
    }
}

function evaluate_lecture1() {

    var total = 0;
    var max_mark = 5;

    // q1
    if ((q1v1.checkState == false) &&
            (q1v2.checkState == true) &&
            (q1v3.checkState == false) &&
            (q1v4.checkState == false)){
        total += 1;
    }

    //q2
    if ((q2v1.checkState == true) &&
            (q2v2.checkState == false) &&
            (q2v3.checkState == false) &&
            (q2v4.checkState == false)){
        total += 1;
    }

    //q3
    var rez_q3 = 0;

    if (q3v1.checkState == true)
        rez_q3 += 0.3;

    if (q3v2.checkState == true)
        rez_q3 -= 0.3;

    if (q3v3.checkState == true)
        rez_q3 += 0.4;

    if (q3v4.checkState == true)
        rez_q3 += 0.3;

    if (q3v5.checkState == true)
        rez_q3 -= 0.3;

    if (rez_q3 >= 0)
        total += rez_q3;

    //q4
    if ((q4v1.checkState == false) &&
            (q4v2.checkState == true) &&
            (q4v3.checkState == false)){
        total += 1;
    }

    //q5
    if ((q5v1.checkState == true) &&
            (q5v2.checkState == false) &&
            (q5v3.checkState == false)){
        total += 1;
    }

    var s = 0;

    if (total  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("lecture", "1", "lecture", "2", total, s);
    refreshLectureModel();
}

function evaluate_lecture2(){
    var total = 0;
    var max_mark = 5;

    // q1
    if ((q1v1.checkState == false) &&
            (q1v2.checkState == false) &&
            (q1v3.checkState == true) &&
            (q1v4.checkState == false)){
        total += 1;
    }

    // q2
    if ((q2v1.checkState == false) &&
            (q2v2.checkState == true) &&
            (q2v3.checkState == false)){
        total += 1;
    }

    //q3
    var rez_q3 = 0;

    if (q3v1.checkState == true)
        rez_q3 += 0.5;

    if (q3v2.checkState == true)
        rez_q3 += 0.5;

    if (q3v3.checkState == true)
        rez_q3 -= 0.5;

    if (q3v4.checkState == true)
        rez_q3 -= 0.5;

    if (q3v5.checkState == true)
        rez_q3 -= 0.5;

    if (rez_q3 >= 0)
        total += rez_q3;

    //q4
    if ((q4v1.checkState == true) &&
        (q4v2.checkState == false) &&
        (q4v3.checkState == false)){
            total += 1;
    }

    //q5
    if ((q5v1.checkState == false) &&
            (q5v2.checkState == true) &&
            (q5v3.checkState == false) &&
            (q5v4.checkState == false))
        total += 1;

    var s = 0;

    if (total  >= max_mark / 2){
        s = 1;
    }


    db.setEvaluateUnit("lecture", "2", "lecture", "3", total, s);
    db.setEvaluateUnit("", "", "practice", "1", 0, s);
    refreshLectureModel();
}

function evaluate_lecture3() {

    var total = 0;
    var max_mark = 5;

    // q1
    if ((q1v1.checkState == false) &&
            (q1v2.checkState == true) &&
            (q1v3.checkState == false) &&
            (q1v4.checkState == false)){
        total += 1;
    }

    // q2
    var rez_q2 = 0

    if (q2v1.checkState == true)
        rez_q2 -= 0.3
    if (q2v2.checkState == true)
        rez_q2 -= 0.3
    if (q2v3.checkState == true)
        rez_q2 -= 0.3
    if (q2v4.checkState == true)
        rez_q2 += 0.3
    if (q2v5.checkState == true)
        rez_q2 += 0.3
    if (q2v6.checkState == true)
        rez_q2 += 0.4
    if (q2v7.checkState == true)
        rez_q2 -= 0.3
    if (q2v8.checkState == true)
        rez_q2 -= 0.3

    if (rez_q2 >= 0)
        total += rez_q2

    // q3
    if ((q3v1.checkState == false) &&
            (q3v2.checkState == false) &&
            (q3v3.checkState == true) &&
            (q3v4.checkState == false)){
        total += 1;
    }

    // q4
    if ((q4v1.checkState == false) &&
            (q4v2.checkState == false) &&
            (q4v3.checkState == true)){
        total += 1;
    }

    // q5
    if ((q5v1.checkState == false) &&
            (q5v2.checkState == false) &&
            (q5v3.checkState == true)){
        total += 1;
    }

    var s = 0;

    if (total  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("lecture", "3", "lecture", "4", total, s);
    db.setEvaluateUnit("", "", "practice", "2", 0, s);
    refreshLectureModel();
}

function evaluate_lecture4() {

    var total = 0;
    var max_mark = 5;


    // q1

    if ((q1v1.checkState == false) &&
            (q1v2.checkState == true) &&
            (q1v3.checkState == false)){
        total += 1;
    }
    // q2

    if ((q2v1.checkState == false) &&
            (q2v2.checkState == true) &&
            (q2v3.checkState == false)){
        total += 1;
    }

    // q3
    if ((q3v1.checkState == true) &&
            (q3v2.checkState == false) &&
            (q3v3.checkState == false)){
        total += 1;
    }

    // q4
    if ((q4v1.checkState == false) &&
            (q4v2.checkState == false) &&
            (q4v3.checkState == true)){
        total += 1;
    }

    // q5
    if ((q5v1.checkState == true) &&
            (q5v2.checkState == false) &&
            (q5v3.checkState == false)){
        total += 1;
    }

    var s = 0;

    if (total  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("lecture", "4", "lecture", "5", total, s);
    db.setEvaluateUnit("", "", "practice", "3", 0, s);
    refreshLectureModel();
}

function evaluate_lecture5() {

    var total = 0;
    var max_mark = 5;


    // q1

    if ((q1v1.checkState == true) &&
            (q1v2.checkState == false) &&
            (q1v3.checkState == false) &&
            (q1v4.checkState == false)){
        total += 1;
    }
    // q2

    if ((q2v1.checkState == false) &&
            (q2v2.checkState == true) &&
            (q2v3.checkState == false)){
        total += 1;
    }

    // q3
    if ((q3v1.checkState == false) &&
            (q3v2.checkState == true) &&
            (q3v3.checkState == false) &&
            (q3v4.checkState == false)){
        total += 1;
    }

    // q4
    if ((q4v1.checkState == false) &&
            (q4v2.checkState == true) &&
            (q4v3.checkState == false) &&
            (q4v4.checkState == false)){
        total += 1;
    }
    // q5
    if ((q5v1.checkState == false) &&
            (q5v2.checkState == true) &&
            (q5v3.checkState == false) &&
            (q5v4.checkState == false)){
        total += 1;
    }

    var s = 0;

    if (total  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("lecture", "5", "lecture", "6", total, s);
    db.setEvaluateUnit("", "", "practice", "4", 0, s);
    db.setEvaluateUnit("", "", "self", "1", 0, s);
    refreshLectureModel();
}

function evaluate_lecture6() {

    var total = 0;
    var max_mark = 5;


    // q1

    if ((q1v1.checkState == true) &&
            (q1v2.checkState == false) &&
            (q1v3.checkState == false) &&
            (q1v4.checkState == false)){
        total += 1;
    }
    // q2

    if ((q2v1.checkState == true) &&
            (q2v2.checkState == false)){
        total += 1;
    }

    // q3
    if ((q3v1.checkState == true) &&
            (q3v2.checkState == false)){
        total += 1;
    }

    // q4
    if ((q4v1.checkState == false) &&
            (q4v2.checkState == true) &&
            (q4v3.checkState == false) &&
            (q4v4.checkState == false)){
        total += 1;
    }
    // q5

    var rez_q5 = 0

    if (q5v1.checkState == true)
        rez_q5 -= 0.5
    if (q5v2.checkState == true)
        rez_q5 += 0.5
    if (q5v3.checkState == true)
        rez_q5 += 0.5
    if (q5v4.checkState == true)
        rez_q5 -= 0.5

    if (rez_q5 >= 0){
        total += rez_q5

    }

    var s = 0;

    if (total  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("lecture", "6", "", "", total, s);
    db.setEvaluateUnit("", "", "self", "2", 0, s);
    refreshLectureModel();
}

function evaluate_practice1(){

    var max_mark = 10;
    var ball = 0

    if (query_1_text.text.toLowerCase() == "create database car_shop;")
        ball += 2;
    if (query_2_text.text.toLowerCase() == "use car_shop;")
        ball += 2;
    if (query_3_text.text.toLowerCase() == "show tables;")
        ball += 1;
    if (query_4_text.text.toLowerCase() == "create table cars (id int primary key not null, maker varchar(20), model varchar(30), year int, color varchar(20));")
        ball += 2;
    if (query_5_text.text.toLowerCase() == "describe cars;")
        ball += 1;
    if (query_6_text.text.toLowerCase() == "drop cars;")
        ball += 2;

    var s = 0;

    if (ball  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("practice", "1", "", "", ball, s);
    refreshPracticeModel();

}

function evaluate_practice2(){

    var max_mark = 10;
    var ball = 0

    if (query_1_text.text.toLowerCase() == "insert into cars values (null, 'zaz', '965', 1965, 'white');")
        ball += 2;
    if (query_2_text.text.toLowerCase() == "insert into cars values ((null, 'vaz', '2101', 1962, 'red'), (null, 'vaz', '21099', 1999, 'chery'));")
        ball += 2;
    if (query_3_text.text.toLowerCase() == "update cars set color = 'green' where maker = 'vaz' and model = '2101';")
        ball += 2;
    if (query_4_text.text.toLowerCase() == "delete from cars where year > 1975;")
        ball += 2;
    if (query_5_text.text.toLowerCase() == "delete from cars;")
        ball += 2;

    var s = 0;

    if (ball  >= max_mark / 2){
        s = 1;
    }


    db.setEvaluateUnit("practice", "2", "", "", ball, s);
    refreshPracticeModel();

}

function evaluate_practice3(){

    var max_mark = 10;
    var ball = 0

    if (query_1_text.text.toLowerCase() == "select * from cars;")
        ball += 2;
    if (query_2_text.text.toLowerCase() == "select maker, model from cars where year = 1962;")
        ball += 2;
    if (query_3_text.text.toLowerCase() == "select * from cars where year < 1990 and model = 'vaz';")
        ball += 2;
    if (query_4_text.text.toLowerCase() == "select distinct maker from cars;")
        ball += 2;
    if (query_5_text.text.toLowerCase() == "select maker from cars group by maker;")
        ball += 2;

    var s = 0;

    if (ball  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("practice", "3", "", "", ball, s);
    refreshPracticeModel();

}

function evaluate_practice4(){

    var max_mark = 10;
    var ball = 0

    if (query_1_text.text.toLowerCase() == "create view as select * from cars;")
        ball += 2;
    if (query_2_text.text.toLowerCase() == "create view as select distinct maker from cars;")
        ball += 2;
    if (query_3_text.text.toLowerCase() == "create view as select * from cars where year < 1990 and model = 'vaz';")
        ball += 3;
    if (query_4_text.text.toLowerCase() == "select * from cars where maker = (select masker from makers where id = 101);")
        ball += 3;

    var s = 0;

    if (ball  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("practice", "4", "", "", ball, s);
    refreshPracticeModel();
}

function evaluate_self1(){

    var max_mark = 15;
    var ball = 0

    // zad 1
    var rez_q1 = 0

    if (q1v1.checkState == true)
        rez_q1 -= 2
    if (q1v2.checkState == true)
        rez_q1 += 2
    if (q1v3.checkState == true)
        rez_q1 -= 2
    if (q1v4.checkState == true)
        rez_q1 += 2

    if (rez_q1 >= 0)
        ball += rez_q1

    // zad 2

    if ((q2v1.checkState == false) &&
            (q2v2.checkState == false) &&
            (q2v3.checkState == true) &&
            (q2v4.checkState == false)){
        ball += 2;
    }

    // zad 3

    if ((q3v1.checkState == true) &&
            (q3v2.checkState == false) &&
            (q3v3.checkState == false) &&
            (q3v4.checkState == false)){
        ball += 2;
    }

    // zad 4


    if ((q4v1.checkState == false) &&
            (q4v2.checkState == false) &&
            (q4v3.checkState == false) &&
            (q4v4.checkState == true)){
        ball += 2;
    }

    // zad 5
    if (query_1_text.text.toLowerCase() == "select count(id) from cars group by id;")
        ball += 5;

    var s = 0;

    if (ball  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("self", "1", "", "", ball, s);
    refreshSelfJobModel();
}

function evaluate_self2(){

    var max_mark = 15;
    var ball = 0

    // zad 1
    if ((q1v1.checkState == false) &&
            (q1v2.checkState == true) &&
            (q1v3.checkState == false)){
        ball += 4;
    }

    // zad 2

    if ((q2v1.checkState == false) &&
            (q2v2.checkState == false) &&
            (q2v3.checkState == true) &&
            (q2v4.checkState == false)){
        ball += 3;
    }

    // zad 3

    if ((q3v1.checkState == true) &&
            (q3v2.checkState == false) &&
            (q3v3.checkState == false)){
        ball += 4;
    }

    // zad 4


    if ((q4v1.checkState == false) &&
            (q4v2.checkState == false) &&
            (q4v3.checkState == false) &&
            (q4v4.checkState == true)){
        ball += 4;
    }

    var s = 0;

    if (ball  >= max_mark / 2){
        s = 1;
    }

    db.setEvaluateUnit("self", "2", "", "", ball, s);
    refreshSelfJobModel();
}

function refreshStatsMainPage(){
    var currentMark = db.getStatisticsMainPage();
    valueWhole.text = currentMark;
}

function refreshStatistics(){
    var params = db.getStatistics();
    total_total.text = params[0]

    lect_total.text = params[1]
    practice_total.text = params[2]
    self_total.text = params[3]

    lect_complited.text = params[4]
    practice_complited.text = params[5]
    self_complited.text = params[6]
    total_complited.text = parseInt(params[4]) + parseInt(params[5]) + parseInt(params[6])

    lect_balls.text = params[7]
    practice_balls.text = params[8]
    self_balls.text = params[9]
    total_balls.text = parseFloat(params[7]) + parseFloat(params[8]) + parseFloat(params[9])


}
