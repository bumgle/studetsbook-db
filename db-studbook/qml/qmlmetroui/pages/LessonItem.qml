import QtQuick 1.1
import CustomComponents 1.0

Rectangle{

    property string headerText: "Header"
    property string bodyText: "Body"
    property double ballValue: 0
    property string view: ""
    property string status: ""

    id: itemBase
    width: 200
    height: 125
    radius: 5
    color: "white"
    clip: true

    Text{
        id: headerText
        anchors.top: itemBase.top
        anchors.topMargin: 5
        anchors.left: itemBase.left
        anchors.leftMargin: 5
        width: itemBase.width
        height: 50
        text: itemBase.headerText
        font.bold: true
        font.family: "Monaco"
        color: "black"
        wrapMode: Text.WordWrap
    }

    Line{
        id: separaror
        x1: 5
        y1: 25
        x2: parent.width - 5
        y2: 25
        color: "black"
        penWidth: 1
    }

    Text{
        id: bodyText
        anchors.top: separaror.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        width: itemBase.width -10
        height: 50
        text: itemBase.bodyText
        font.family: "Arial"
        wrapMode: Text.WordWrap
        color: "black"
    }


    MouseArea{
        anchors.fill: itemBase
        hoverEnabled: true
        onEntered: { shadow.anchors.topMargin = -30; itemBase.color = "yellow" }
        onExited: { shadow.anchors.topMargin = 30; itemBase.color = "white" }
        onClicked: {
            if (itemBase.status == "1"){
                var lesson = Qt.createComponent(itemBase.view)
                if(lesson.status === Component.Ready){
                   var view = lesson.createObject(root);
                   leftMenu.x = leftMenu.x - leftMenu.width
                   lecturePg.opacity = 0
                   quitButton.opacity = 0
                }
            }
        }
    }

    Rectangle{
        id: shadow
        width: itemBase.width
        height: 95
        anchors.left: itemBase.left
        anchors.top: itemBase.verticalCenter
        anchors.topMargin: 30
        color: "black"
        opacity: 1

        StarsIndicator{
            id: starIndicator
            anchors.top: shadow.top
            anchors.topMargin: 3
            anchors.horizontalCenter: shadow.horizontalCenter
            ball: ballValue
        }

        Rectangle{
            id: textIndicator
            anchors.top: starIndicator.bottom
            anchors.topMargin: 15
            anchors.horizontalCenter: shadow.horizontalCenter
            width: 120
            height: 40
            color: "black"

            Text{
                id: textIndiacaorText
                anchors.top: textIndicator.top
                anchors.topMargin: 5
                anchors.verticalCenter: textIndicator.verticalCenter
                anchors.horizontalCenter: textIndicator.horizontalCenter
                color: "white"
                font.pixelSize: 20
                text: ""
            }
        }

        Behavior on anchors.topMargin {
            PropertyAnimation{
                duration: 300
            }
        }
    }

    Component.onCompleted:{
        if (itemBase.status == "1"){
            textIndiacaorText.color = "#2DED21"
            textIndiacaorText.text = "Доступно"
        }
        else{
            textIndiacaorText.color = "#FF4637"
            textIndiacaorText.text = "Не доступно"
        }

    }
}
