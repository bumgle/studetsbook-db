import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "#04A7FF"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Лекция 5</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Представления (VIEW)</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\t<b>Представление (VIEW)</ b> - объект данных который не содержит никаких данных его владельца. Это - тип таблицы, чье содержание выбирается из других таблиц с помощью выполнения запроса. Поскольку значения в этих таблицах меняются, то авто- матически, их значения могут быть показаны представлением. В этой главе, вы узнаете что такое представления, как они создаются, и не- много об их возможностях и ограничениях. Использование представлений основанных на улучшенных средствах запросов, таких как объединение и под- запрос, разработанных очень тщательно, в некоторых случаях даст больший выигрыш по сравнению с запросами.<br><br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "\t<b>ЧТО ТАКОЕ ПРЕДСТАВЛЕНИЕ?</b><br><br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{

                        text: "\tТипы таблиц, с которыми вы имели дело до сих пор, назывались - базовыми таблицами. Это - таблицы, которые содержат данные. Однако имеется другой вид таблиц: - представления. Представления - это таблицы чье содержание выбирается или получается из других таблиц. Они работают в запросах и операторах DML точно также как и основные таблицы, но не содержат ника- ких собственных данных. Представления - подобны окнам, через которые вы просматриваете информа- цию( как она есть, или в другой форме, как вы потом увидите ), которая фактически хранится в базовой таблице. Представление - это фактически запрос, который выполняется всякий раз, когда представление становится темой ко- манды. Вывод запроса при этом в каждый момент становится содержанием представления.<br><br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "\t<b>КОМАНДА CREATE VIEW</b><br><br> \tВы создаете представление командой <b>CREATE VIEW</b>. Она состоит из слов <b>CREATE VIEW</b> (СОЗДАТЬ ПРЕДСТАВЛЕНИЕ), имени представления которое нужно создать, слова <b>AS</b> (КАК), и далее запроса, как в следующем примере:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }


                    CodeExample{
                        exampleText: "<b>CREATE VIEW Londonstaff AS SELECT * FROM Salespeople WHERE city = 'London';</b>"
                    }


                    Text{
                        text: "\tТеперь Вы имеете представление, называемое Londonstaff. Вы можете использовать это представление точно так же как и любую другую таблицу. Она может быть запрошена, модифицирована, вставлена в, удалена из, и соединена с, другими таблицами и представлениями. Давайте сделаем запрос такого представления:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }


                    CodeExample{
                        exampleText: "<b>SELECT * FROM Londonstaff;</b>"
                    }

                    Text{
                        text: "\tПредставление Londonstaff Когда вы приказываете SQL выбрать<b>(SELECT)</b> все строки <b>( * )</b> из представления, он выполняет запрос содержащий в определении - Loncfonstaff, и возвращает все из его вывода. Имея предикат в запросе представления, можно вывести только те строки из представления, которые будут удовлетворять этому предикату. Преимущество использования представления, по сравнению с основной таблицы, в том, что представление будет модифицировано автоматически всякий раз, когда таблица лежащая в его основе изменяется. Содержание представления не фиксировано, и переназначается каждый раз когда вы ссылаетесь на представление в команде. Если вы добавите завтра другого, живущего в Лондоне продавца, он автоматически появится в представлении.<br>\tПредставления значительно расширяют управление вашими данными. Это - превосходный способ дать публичный доступ к некоторой, но не всей инфор- мации в таблице. Если вы хотите чтобы ваш продавец был показан в таблице Продавцов, но при этом не были показаны комиссии других продавцов, вы могли бы создать представление с использованием следующего оператора"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>CREATE VIEW Salesown AS SELECT snum, sname, city FROM Salespeople:</b>"
                    }

                    Text{
                        text: "<br>\t<b>МОДИФИЦИРОВАНИЕ ПРЕДСТАВЛЕНИЙ</b><br><br> \tПредставление может теперь изменяться командами модификации DML, но модификация не будет воздействовать на само представление. Команды будут на самом деле перенаправлены к базовой таблице:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>UPDATE Salesown SET city = 'Palo Alto' WHERE snum = 1004;</b>"
                    }

                    Text{
                        text: "\tЕго действие идентично выполнению той же команды в таблице Продавцов. Однако, если значение комиссионных продавца будет обработано командой <b>UPDATE</b>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>UPDATE Salesown SET comm = .20 WHERE snum = 1004;</b>"
                    }

                    Text{
                        text: "она будет отвергнута, так как поле comm отсутствует в представлении Salesown. Это важное замечание, показывающее что не все представления могут быть модифицированы. Мы будем исследовать проблемы модификации представлений.<br><br> \t<b>ИМЕНОВАНИЕ СТОЛБЦОВ</b><br><br> \tВ нашем примере, пол наших представлений имеют свои имена, полученные прямо из имен полей основной таблицы. Это удобно. Однако, иногда вам нужно снабжать ваши столбцы новыми именами:<br>\t- когда некоторые столбцы являются выводимыми, и поэтому не имеющими имен.\t - когда два или более столбцов в объединении, имеют те же имена что в их базовой таблице.<br>\tИмена, которые могут стать именами полей, даются в круглых скобках <b>( )</b>, после имени таблиц. Они не будут запрошены, если совпадают с именами полей запрашиваемой таблицы. Тип данных и размер этих полей будут отличаться от запрашиваемых полей которые <i>передаются</i> в них. Обычно вы не указываете новых имен полей, но если вы все таки сделали это, вы дол- жны делать это для каждого пол в представлении."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "\t<b>КОМБИНИРОВАНИЕ ПРЕДИКАТОВ ПРЕДСТАВЛЕНИЙ И ОСНОВНЫХ ЗАПРОСОВ В ПРЕДСТАВЛЕНИЯХ</b><br>\tКогда вы делаете запрос представления, вы собственно, запрашиваете запрос. Основной способ для SQL обойти это, - объединить предикаты двух запросов в один. Давайте посмотрим еще раз на наше представление с име- нем Londonstaff:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>CREATE VIEW Londonstaff AS SELECT * FROM Salespeople WHERE city = 'London';</b>"
                    }

                    Text{
                        text: "\tЕсли мы выполняем следующий запрос в этом представлении"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM Londonstaff WHERE comm > .12;</b>"
                    }

                    Text{
                        text: "он такой же как если бы мы выполнили следующее в таблице Продавцов:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM Salespeople WHERE city = 'London' AND comm > .12;</b>"
                    }

                    Text{
                        text: "\tЭто прекрасно, за исключением того, что появляется возможна проблема с представлением. Имеется возможность комбинации из двух полностью допустимых предикатов и получения предиката который не будет работать. Например, предположим что мы создаем (CREATE) следующее представ- ление:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>CREATE VIEW Ratingcount (rating, number) AS SELECT rating, COUNT (*) FROM Customers GROUP BY rating;</b>"
                    }

                    Text{
                        text: "\tЭто дает нам число заказчиков которые мы имеем для каждого уровня оценки(rating). Вы можете затем сделать запрос этого представления чтобы выяснить, имеется ли какая-нибудь оценка, в настоящее время назначенная для трех заказчиков:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT * FROM Ratingcount WHERE number = 3;</b>"
                    }

                    Text{
                        text: "\tПосмотрим что случится если мы скомбинируем два предиката:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT rating, COUNT (*) FROM Customers WHERE COUNT (*) = 3 GROUP BY rating;</b>"
                    }



                    Text{
                        text: "\tЭто недопустимый запрос. Агрегатные функции, такие как <b>COUNT</b> (СЧЕТ), не могут использоваться в предикате. Правильным способом при формировании вышеупомянутого запроса, конеч- но же будет следующий:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>SELECT rating, COUNT (*) FROM Customers GROUP BY rating; HAVING COUNT (*) = 3;</b>"
                    }

                    Text{
                        text: "\tНо SQL может не выполнить превращения. Может ли равноценный запрос вместо запроса Ratingcount потерпеть неудачу? Да может! Это - неоднозначна область SQL, где методика использования представ- лений может дать хорошие результаты. Самое лучшее что можно сделать в случае, когда об этом ничего не ска- зано в вашей системной документации, так это попытка в ней разобрать- с. Если команда допустима, вы можете использовать представления чтобы установить некоторые ограничения SQL в синтаксисе запроса.<br><br>\t<b>ЧТО НЕ МОГУТ ДЕЛАТЬ ПРЕДСТАВЛЕНИЯ</b><br><br> \tИмеются большое количество типов представлений ( включая многие из наших примеров в этой главе ) которые являются доступными только для чтения. Это означает, что их можно запрашивать, но они не могут подвергаться действиям команд модификации. ( Мы будем рассматривать эту тему в Главе 21. ) Имеются также некоторые виды запросов, которые не допустимы в определениях представлений. Одиночное представление должно основываться на одиночном запросе; <b>ОБЪЕДИНЕНИЕ (UNION)</b> и <b>ОБЪЕДИНЕНИЕ ВСЕГО (UNIOM ALL)</b> не разрешаются. <b>УПОРЯДОЧЕНИЕ ПО(ORDER BY)</b> никогда не используется в опреде- лении представлений. Вывод запроса формирует содержание представления, которое напоминает базовую таблицу и является - по определению - неупорядоченным.<br><br>\t<b>УДАЛЕНИЕ ПРЕДСТАВЛЕНИЙ</b><br><br>\tСинтаксис удаления представления из базы данных подобен синтаксису удаления базовых таблиц:"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    CodeExample{
                        exampleText: "<b>DROP VIEW view name</b>"
                    }

                    Text{
                        text: "\tВ этом нет необходимости, однако, сначала надо удалить все содержание как это делается с базовой таблицей, потому что содержание представления не является созданным и сохраняется в течении определенной команды. Базовая таблица из которой представление выводится, не эффективна когда представ- ление удалено. Помните, вы должны являться владельцем представления чтобы иметь воз- можность удалить его."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Контрольные вопросы:</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    // question 1
                    LessonSingleQuestion{
                        id: q1
                        question : "Что такое представление?"
                    }

                    SingleQuestionVariant{
                        id: q1v1
                        variantText: "Это объект базы данных, который отображает данные но не содержит их." // *
                    }
                    SingleQuestionVariant{
                        id: q1v2
                        variantText: "Представление это таблица, которая содержит данные, но в отличие от обычной таблицы имеет инструменты для отображения данных."
                    }
                    SingleQuestionVariant{
                        id: q1v3
                        variantText: "Это оператор который создает таблицы."
                    }
                    SingleQuestionVariant{
                        id: q1v4
                        variantText: "Это оператор который создает выборки."
                    }

                    // question 2

                    LessonSingleQuestion{
                        id: q2
                        question : "Для чего предназначены представления?"
                    }

                    SingleQuestionVariant{
                        id: q2v1
                        variantText: "Для сохранения результатов произведенной выборки в виде таблиц. Данные представления сохраняються на момент создания представления." // *
                    }
                    SingleQuestionVariant{
                        id: q2v2
                        variantText: "Для сохранения результатов произведенной выборки в виде таблиц. Представляемые данные не зависят от времени создания представления и изменяються в зависимости от изменения данных в связанных с представлением таблицах." // +
                    }
                    SingleQuestionVariant{
                        id: q2v3
                        variantText: "Для создания и заполнения таблиц."
                    }

                    // question 3

                    LessonSingleQuestion{
                        id: q3
                        question : "Какая команда создает представление?"
                    }

                    SingleQuestionVariant{
                        id: q3v1
                        variantText: "CREATE TABLE"
                    }
                    SingleQuestionVariant{
                        id: q3v2
                        variantText: "CREATE VIEW" // +
                    }
                    SingleQuestionVariant{
                        id: q3v3
                        variantText: "CREATE OBSERVATION"
                    }
                    SingleQuestionVariant{
                        id: q3v4
                        variantText: "CREATE SELECT"
                    }

                    // question 4

                    LessonSingleQuestion{
                        id: q4
                        question : "Какая команда создает представление?"
                    }

                    SingleQuestionVariant{
                        id: q4v1
                        variantText: "CREATE TABLE"
                    }
                    SingleQuestionVariant{
                        id: q4v2
                        variantText: "CREATE VIEW" // *
                    }
                    SingleQuestionVariant{
                        id: q4v3
                        variantText: "CREATE OBSERVATION"
                    }
                    SingleQuestionVariant{
                        id: q4v4
                        variantText: "CREATE SELECT"
                    }

                    // question 5

                    LessonSingleQuestion{
                        id: q5
                        question : "Какая команда предназначена для удаления представления?"
                    }

                    SingleQuestionVariant{
                        id: q5v1
                        variantText: "DELETE VIEW"
                    }
                    SingleQuestionVariant{
                        id: q5v2
                        variantText: "DROP VIEW" // *
                    }
                    SingleQuestionVariant{
                        id: q5v3
                        variantText: "DROP OBSERVATION"
                    }
                    SingleQuestionVariant{
                        id: q5v4
                        variantText: "DELETE SELECT"
                    }


                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_lecture5()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "#04A7FF"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
