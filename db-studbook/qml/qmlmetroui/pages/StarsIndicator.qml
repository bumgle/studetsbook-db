import QtQuick 1.1

Item{
    property double ball: 0

    id: indicator
    width: 100
    height: 20
    clip: true

    Image{
        id: star1
        anchors.left: indicator.left
        anchors.verticalCenter: indicator.verticalCenter
        width: 20
        height: 20
        source: "qrc:/pics/pictures/star.png"
    }
    Image{
        id: star2
        anchors.left: star1.right
        anchors.verticalCenter: indicator.verticalCenter
        width: 20
        height: 20
        source: "qrc:/pics/pictures/star.png"
    }
    Image{
        id: star3
        anchors.left: star2.right
        anchors.verticalCenter: indicator.verticalCenter
        width: 20
        height: 20
        source: "qrc:/pics/pictures/star.png"
    }
    Image{
        id: star4
        anchors.left: star3.right
        anchors.verticalCenter: indicator.verticalCenter
        width: 20
        height: 20
        source: "qrc:/pics/pictures/star.png"
    }
    Image{
        id: star5
        anchors.left: star4.right
        anchors.verticalCenter: indicator.verticalCenter
        width: 20
        height: 20
        source: "qrc:/pics/pictures/star.png"
    }

    Rectangle{
        id: cover
        width: indicator.width
        height: indicator.height
        color: "black"
        anchors.top: indicator.top
        x:  indicator.x + (star1.width * ball) - 50
    }
}
