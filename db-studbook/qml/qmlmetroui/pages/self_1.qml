import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "orange"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Самомтоятельная работа 1</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Групповые операции</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "\tСамостоятельно изучите материал о групповых операциях которые могут быть выполнены с модификатором <b>GROUPE BY</b> оператора SELECT."
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 40
                    }

                   Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                   }

                   Text{
                        text: "<b>Задания</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                   }

                   // zad 1
                   LessonSingleQuestion{
                       id: q1
                       question : "Какие групповые операции можно выполнять совместно с оператором GROUPE BY?"
                   }

                   SingleQuestionVariant{
                       id: q1v1
                       variantText: "SIN"
                   }

                   SingleQuestionVariant{
                       id: q1v2
                       variantText: "COUNT" //+
                   }

                   SingleQuestionVariant{
                       id: q1v3
                       variantText: "COS"
                   }

                   SingleQuestionVariant{
                       id: q1v4
                       variantText: "SUM" //+
                   }

                   // zad 2
                   LessonSingleQuestion{
                       id: q2
                       question : "Для чего используеться функция COUNT()?"
                   }

                   SingleQuestionVariant{
                       id: q2v1
                       variantText: "Для суммы всех сгруппированных значений в столбце."
                   }

                   SingleQuestionVariant{
                       id: q2v2
                       variantText: "Для суммы всех сгруппированных значений в строке."
                   }

                   SingleQuestionVariant{
                       id: q2v3
                       variantText: "Для счета количества сгруппированных значений." // +
                   }

                   SingleQuestionVariant{
                       id: q2v4
                       variantText: "Для поиска среднего арифметического сгруппированных значений."
                   }


                   // zad 3

                   LessonSingleQuestion{
                       id: q3
                       question : "Для чего используеться функция SUM()?"
                   }

                   SingleQuestionVariant{
                       id: q3v1
                       variantText: "Для суммы всех сгруппированных значений в столбце." // +
                   }

                   SingleQuestionVariant{
                       id: q3v2
                       variantText: "Для суммы всех сгруппированных значений в строке."
                   }

                   SingleQuestionVariant{
                       id: q3v3
                       variantText: "Для счета количества сгруппированных значений."
                   }

                   SingleQuestionVariant{
                       id: q3v4
                       variantText: "Для поиска среднего арифметического сгруппированных значений."
                   }

                   // zad 4

                   LessonSingleQuestion{
                       id: q4
                       question : "Для чего используеться функция AVG()?"
                   }

                   SingleQuestionVariant{
                       id: q4v1
                       variantText: "Для суммы всех сгруппированных значений в столбце."
                   }

                   SingleQuestionVariant{
                       id: q4v2
                       variantText: "Для суммы всех сгруппированных значений в строке."
                   }

                   SingleQuestionVariant{
                       id: q4v3
                       variantText: "Для счета количества сгруппированных значений."
                   }

                   SingleQuestionVariant{
                       id: q4v4
                       variantText: "Для поиска среднего арифметического сгруппированных значений." //+
                   }

                   // zad 5

                   Text{
                       text: "\tПодсчитайте количество записей в таблице cars, используя колонку id:"
                       color: "black"
                       width: pageContent.width - 20
                       wrapMode: Text.WordWrap
                       textFormat: Text.StyledText
                       horizontalAlignment: Text.AlignJustify
                       font.pixelSize: 40
                   }

                   Rectangle{
                       id: query_1_base
                       color: "#bfddf3"
                       width: pageContent.width - 50
                       height: 50

                       TextInput{
                           id: query_1_text
                           anchors.fill: parent
                           anchors.leftMargin: 20
                           anchors.rightMargin: 20
                           horizontalAlignment: TextInput.AlignLeft
                           font.pixelSize: 40
                           font.bold: true
                           color: "black"
                       }
                    }


                   Text{
                        text: "<br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_self1()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "orange"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
