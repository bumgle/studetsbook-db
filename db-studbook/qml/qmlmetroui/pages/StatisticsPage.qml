import QtQuick 1.1
import Bridge 1.0
import CustomComponents 1.0
import "pagescripts.js" as PageScripts

Item{
    property int originX
    property int originY

    id: statsBase

    Database{
        id: db
    }

    Behavior on opacity{
        NumberAnimation{duration: 200}
    }

    Text{
        id: bookName
        x: 10
        y: 25
        text: qsTr("Статистика")
        color: "white"
        font.family: "Monaco"
        font.bold: true
        font.pixelSize: 50
    }

    Line{
        id: separaror
        x1: 0
        y1: 100
        x2: root.width - leftMenu.width - 10 * 3
        y2: 100
        color: "white"
        penWidth: 2
    }

    // голова
    Text{
        x:  -19
        y: 140
        text: "Вид работы"
        color: "white"
        font.pixelSize: 20
    }

    Rectangle {
        id: rectangle1
        x: 20
        y: 200
        width: 335
        height: 51
        color: "#04a7ff"

        Text {
            id: text1
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("Лекции:")
            font.bold: true
            wrapMode: Text.WordWrap
            font.pixelSize: 20
        }
    }

    Rectangle {
        id: rectangle2
        x: 20
        y: 272
        width: 335
        height: 51
        color: "#ff0000"

        Text {
            id: text2
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("Практика:")
            font.bold: true
            font.pixelSize: 20
        }
    }

    Rectangle {
        id: rectangle3
        x: 20
        y: 343
        width: 335
        height: 51
        color: "#ffa500"

        Text {
            id: text3
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("Самостоятельная работа:")
            font.bold: true
            font.pixelSize: 20
        }
    }

    Rectangle {
        id: rectangle4
        x: 402
        y: 200
        width: 67
        height: 51
        color: "#04a7ff"
        Text {
            id: lect_total
            x: 6
            y: 10
            width: 56
            height: 32
            color: "#ffffff"
            text: qsTr("*")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignTop
            font.pixelSize: 20
            wrapMode: Text.WordWrap
            font.bold: true
        }
    }

    Rectangle {
        id: rectangle6
        x: 402
        y: 272
        width: 67
        height: 51
        color: "#ff0000"
        Text {
            id: practice_total
            x: 5
            y: 14
            width: 55
            height: 24
            color: "#ffffff"
            text: qsTr("*")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 20
            font.bold: true
        }
    }

    Rectangle {
        id: rectangle5
        x: 515
        y: 200
        width: 67
        height: 51
        color: "#04a7ff"
        Text {
            id: lect_complited
            x: 6
            y: 10
            width: 56
            height: 32
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            wrapMode: Text.WordWrap
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignTop
        }
    }

    Rectangle {
        id: rectangle7
        x: 515
        y: 272
        width: 67
        height: 51
        color: "#ff0000"
        Text {
            id: practice_complited
            x: 5
            y: 14
            width: 55
            height: 24
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: rectangle8
        x: 402
        y: 343
        width: 67
        height: 51
        color: "#ffa500"
        Text {
            id: self_total
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("*")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 20
            font.bold: true
        }
    }

    Rectangle {
        id: rectangle9
        x: 515
        y: 343
        width: 67
        height: 51
        color: "#ffa500"
        Text {
            id: self_complited
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: rectangle10
        x: 20
        y: 415
        width: 335
        height: 51
        color: "#cd3278"
        Text {
            id: text10
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("Общие показатели:")
            font.pixelSize: 20
            font.bold: true
        }
    }

    Rectangle {
        id: rectangle11
        x: 402
        y: 415
        width: 67
        height: 51
        color: "#cd3278"
        Text {
            id: total_total
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: rectangle12
        x: 515
        y: 415
        width: 67
        height: 51
        color: "#cd3278"
        Text {
            id: total_complited
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Text {
        x: 382
        y: 140
        color: "#ffffff"
        text: "Всего"
        font.pixelSize: 20
    }

    Rectangle {
        id: rectangle13
        x: 625
        y: 200
        width: 67
        height: 51
        color: "#04a7ff"
        Text {
            id: lect_balls
            x: 6
            y: 10
            width: 56
            height: 32
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            wrapMode: Text.WordWrap
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignTop
        }
    }

    Rectangle {
        id: rectangle14
        x: 625
        y: 272
        width: 67
        height: 51
        color: "#ff0000"
        Text {
            id: practice_balls
            x: 5
            y: 14
            width: 55
            height: 24
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: rectangle15
        x: 625
        y: 343
        width: 67
        height: 51
        color: "#ffa500"
        Text {
            id: self_balls
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: rectangle16
        x: 625
        y: 415
        width: 67
        height: 51
        color: "#cd3278"
        Text {
            id: total_balls
            x: 24
            y: 14
            color: "#ffffff"
            text: qsTr("*")
            font.pixelSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Text {
        x: 483
        y: 140
        color: "#ffffff"
        text: "Пройдено"
        font.pixelSize: 20
    }

    Text {
        x: 606
        y: 140
        color: "#ffffff"
        text: "Оценка"
        font.pixelSize: 20
    }


    Component.onCompleted: {
        statsBase.originX = statsBase.x
        statsBase.originY = statsBase.y
        db.initDatabase()
        PageScripts.refreshStatistics()
    }

}
