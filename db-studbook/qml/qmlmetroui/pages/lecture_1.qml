import QtQuick 1.1
import Bridge 1.0
import "../components"
import "pagescripts.js" as PageScripts

Rectangle {

    Bridge{
        id: bridge
    }

    Parser{
        id: parser
    }

    id: lessonView
    width: bridge.screeneWidth()
    height: bridge.screeneHeight()
    x: bridge.screeneWidth() + 10
    y: bridge.screeneHeight() + 10
    opacity: 0
    color: "#04A7FF"




    Rectangle{
        id: pageContent
        anchors.left: closeLessonButton.right
        anchors.leftMargin: 5
        anchors.top: lessonView.top
        anchors.topMargin: 25
        anchors.bottom: lessonView.bottom
        anchors.bottomMargin: 10
        anchors.right: lessonView.right
        anchors.rightMargin: 15
        width: lessonView.width - 20
        height: lessonView.height - 75
        color: "white"
        radius: 5
        clip: true

        Flickable{
            id: flicableArea
            flickableDirection: Flickable.VerticalFlick
            anchors.fill: pageContent
            contentWidth: pageContent.width - 50
            contentHeight: lectureContent.height + 20
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            anchors.leftMargin: 10
            anchors.rightMargin: 10

            Rectangle{

                Column{
                    id: lectureContent
                    width: pageContent.width

                    Text{
                        text: "<b>Лекция 1</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "<b>Введение в базы данных. Общие положения и понятия.</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "    <b>База данных</b> — представленная в объективной форме совокупность" +
                        "самостоятельных материалов (статей, расчётов, нормативных актов, судебных решений" +
                        "и иных подобных материалов), систематизированных таким образом, чтобы эти материалы" +
                        "могли быть найдены и обработаны с помощью электронной вычислительной машины (ЭВМ)" +
                        "(Гражданский кодекс РФ, ст. 1260)." +
                        "    Cуществует огромное количество разновидностей баз данных, отличающихся по различным критериям. Например, в" +
                        "«Энциклопедии технологий баз данных», по материалам которой написан данный раздел, определяются свыше 50 видов БД." +
                        "Основные классификации приведены ниже.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<b>Классификация по модели данных</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }


                    Text{
                        text: "- Иерархическая<br>- Сетевая<br>- Реляционная<br>- Объектная и объектно-ориентированная<br>- Объектно-реляционная<br>- Функциональная<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<b>Классификация по среде постоянного хранения</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    Text{
                        text: "- Во вторичной памяти, или традиционная (англ. conventional database): средой постоянного хранения является периферийная энергонезависимая память (вторичная память) — как правило жёсткий диск.<br>- В оперативную память СУБД помещает лишь кеш и данные для текущей обработки.<br>- В оперативной памяти (англ. in-memory database, memory-resident database, main memory database): все данные на стадии исполнения находятся в оперативной памяти.<br>- В третичной памяти (англ. tertiary database): средой постоянного хранения является отсоединяемое от сервера устройство массового хранения (третичная память), как правило на основе магнитных лент или оптических дисков.<br>- Во вторичной памяти сервера хранится лишь каталог данных третичной памяти, файловый кеш и данные для текущей обработки; загрузка же самих данных требует специальной процедуры.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<b>Классификация по содержимому</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }
                    Text{
                        text: "- Географическая<br>- Историческая<br>- Научная<br> -Мультимедийная.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }
                    Text{
                        text: "<b>Классификация по степени распределённости</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }
                    Text{
                        text: "- Централизованная, или сосредоточенная (англ. centralized database): БД, полностью поддерживаемая на одном компьютере.<br>- Распределённая (англ. distributed database): БД, составные части которой размещаются в различных узлах компьютерной сети в соответствии с каким-либо критерием.<br>- Неоднородная (англ. heterogeneous distributed database): фрагменты распределённой БД в разных узлах сети поддерживаются средствами более одной СУБД<br>- Однородная (англ. homogeneous distributed database): фрагменты распределённой БД в разных узлах сети поддерживаются средствами одной и той же СУБД.<br>- Фрагментированная, или секционированная (англ. partitioned database): методом распределения данных является фрагментирование (партиционирование, секционирование), вертикальное или горизонтальное.<br>- Тиражированная (англ. replicated database): методом распределения данных является тиражирование (репликация).<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }
                    Text{
                        text: "<b>Другие виды БД</b><br>"
                        font.pixelSize: 20
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }
                    Text{
                        text: "- Пространственная (англ. spatial database): БД, в которой поддерживаются пространственные свойства сущностей предметной области. Такие БД широко используются в геоинформационных системах.<br>- Временная, или темпоральная (англ. temporal database): БД, в которой поддерживается какой-либо аспект времени, не считая времени, определяемого пользователем.<br>- Пространственно-временная (англ. spatial-temporal database) БД: БД, в которой одновременно поддерживается одно или более измерений в аспектах как пространства, так и времени.<br>- Циклическая (англ. round-robin database): БД, объём хранимых данных которой не меняется со временем, поскольку в процессе сохранения данных одни и те же записи используются циклически.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }
                    Text{
                        text: "    <b>Реляционная база данных</b> — база данных, основанная на реляционной модели данных. Слово «реляционный» происходит от англ. relation. Для работы с реляционными БД применяют реляционные СУБД. Использование реляционных баз данных было предложено доктором Коддом из компании IBM в 1970 году.<br><br>    <B>Система управления базами данных (СУБД)</b> — совокупность программных и лингвистических средств общего или специального назначения, обеспечивающих управление созданием и использованием баз данных.<br><br><b>Основные функции СУБД:</b><br>- управление данными во внешней памяти (на дисках);<br>- управление данными в оперативной памяти с использованием дискового кэша;<br>- журнализация изменений, резервное копирование и восстановление базы данных после сбоев;<br>- поддержка языков БД (язык определения данных, язык манипулирования данными).<br><br><b>Обычно современная СУБД содержит следующие компоненты:</b><br>- ядро, которое отвечает за управление данными во внешней и оперативной памяти, и журнализацию;<br>- процессор языка базы данных, обеспечивающий оптимизацию запросов на извлечение и изменение данных и создание, как правило, машинно-независимого исполняемого внутреннего кода;<br>- подсистему поддержки времени исполнения, которая интерпретирует программы манипуляции данными, создающие пользовательский интерфейс с СУБД,а также сервисные программы (внешние утилиты), обеспечивающие ряд дополнительных возможностей по обслуживанию информационной системы.<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }
                    Text{
                        text: "    <b>Реляционная СУБД (РСУБД; иначе Система управления реляционными базами данных, СУРБД)</b> — СУБД, управляющая реляционными базами данных. Понятие реляционный (англ. relation — отношение) связано с разработками известного английского специалиста в области систем баз данных Эдгара Кодда (Edgar Codd). Эти модели характеризуются простотой структуры данных, удобным для пользователя табличным представлением и возможностью использования формального аппарата алгебры отношений и реляционного исчисления для обработки данных. Реляционная модель ориентирована на организацию данных в виде двумерных таблиц. Каждая реляционная таблица представляет собой двумерный массив и обладает следующими свойствами:<br>- каждый элемент таблицы — один элемент данных<br>- все ячейки в столбце таблицы однородные, то есть все элементы в столбце имеют одинаковый тип (числовой, символьный и т. д.)<br>- каждый столбец имеет уникальное имя<br>- одинаковые строки в таблице отсутствуют<br>- порядок следования строк и столбцов может быть произвольным<br>"
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignJustify
                        font.pixelSize: 20
                    }

                    Text{
                        text: "<b>Контрольные вопросы:</b><br>"
                        font.pixelSize: 40
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    // question 1
                    LessonSingleQuestion{
                        id: q1
                        question : "Что такое база данных?"
                    }

                    SingleQuestionVariant{
                        id: q1v1
                        variantText: "Совокупность знаний"
                    }

                    SingleQuestionVariant{
                        id: q1v2
                        variantText: "Систематизированная и структурированная совокупность знаний" //+
                    }

                    SingleQuestionVariant{
                        id: q1v3
                        variantText: "Связанные таблицы"
                    }

                    SingleQuestionVariant{
                        id: q1v4
                        variantText: "Группа не связанных таблиц"
                    }

                    //question 2
                    LessonSingleQuestion{
                        id: q2
                        question : "Кем была предложена релиационная модель данных?"
                    }

                    SingleQuestionVariant{
                        id: q2v1
                        variantText: "Коддом" //+
                    }

                    SingleQuestionVariant{
                        id: q2v2
                        variantText: "Торсальдсом"
                    }

                    SingleQuestionVariant{
                        id: q2v3
                        variantText: "Энштейном"
                    }

                    SingleQuestionVariant{
                        id: q2v4
                        variantText: "Джобсом"
                    }


                    //question 3
                    LessonSingleQuestion{
                        id: q3
                        question : "Назавите формы организации информации в базе данных?"
                    }

                    SingleQuestionVariant{
                        id: q3v1
                        variantText: "Функциональная" //+
                    }

                    SingleQuestionVariant{
                        id: q3v2
                        variantText: "Табличная"
                    }

                    SingleQuestionVariant{
                        id: q3v3
                        variantText: "Объектная" //+
                    }

                    SingleQuestionVariant{
                        id: q3v4
                        variantText: "Иерархическая" //+
                    }
                    SingleQuestionVariant{
                        id: q3v5
                        variantText: "Схематическая"
                    }

                    //question 4
                    LessonSingleQuestion{
                        id: q4
                        question : "Дайте определение реляционной БД?"
                    }

                    SingleQuestionVariant{
                        id: q4v1
                        variantText: "База данных основной структурной еденицой которой являеться таблица" //+
                    }

                    SingleQuestionVariant{
                        id: q4v2
                        variantText: "База данных основной структурной еденицой которой являеться дерево"
                    }

                    SingleQuestionVariant{
                        id: q4v3
                        variantText: "База данных основной структурной еденицой которой являеться граф"
                    }

                    //question 5
                    LessonSingleQuestion{
                        id: q5
                        question : "Какое предназначение СУБД?"
                    }

                    SingleQuestionVariant{
                        id: q5v1
                        variantText: "Предаставлять возможность управления базами данных и информацией" //+
                    }

                    SingleQuestionVariant{
                        id: q5v2
                        variantText: "Защищать информацию находящуюся в базах данных"
                    }

                    SingleQuestionVariant{
                        id: q5v3
                        variantText: "Диагностировать состояние баз данных"
                    }

                    Text{
                        text: "<br>"
                        font.pixelSize: 30
                        font.bold: true
                        horizontalAlignment: Text.Center
                        textFormat: Text.StyledText
                        color: "black"
                        width: pageContent.width - 20
                        wrapMode: Text.WordWrap
                    }

                    MetroToolButton{
                      id: submit
                      width: 220
                      height: 55
                      color: "green"
                      anchors.horizontalCenter: lectureContent.horizontalCenter
                      headerText: qsTr("Отправить ответ")

                      MouseArea{
                          anchors.fill: parent
                          onClicked: {
                              lessonView.x = bridge.screeneWidth() + 10
                              lessonView.y = bridge.screeneHeight() + 10
                              lessonView.destroy()
                              leftMenu.x = leftMenu.originX
                              quitButton.opacity = 1
                              lecturePg.opacity = 1
                              PageScripts.evaluate_lecture1()
                          }
                      }
                    }
                }
            }
       }
}

    MetroToolButton{
        id: closeLessonButton
        width: 55
        height: 55
        color: "#04A7FF"
        x: 10
        y: 25
        headerText: qsTr("")
        buttonIcon: "qrc:/pics/pictures/cancel.png"

        MouseArea{
            anchors.fill: parent
            onClicked: {
                lessonView.x = bridge.screeneWidth() + 10
                lessonView.y = bridge.screeneHeight() + 10
                lessonView.destroy()
                leftMenu.x = leftMenu.originX
                quitButton.opacity = 1
                lecturePg.opacity = 1
            }
        }
    }

    Behavior on x {
        NumberAnimation{duration: 500}
    }

    Behavior on y {
        NumberAnimation{duration: 500}
    }

    Behavior on opacity{
        NumberAnimation{duration: 700}
    }

    Component.onCompleted: {
       lessonView.x = 0
       lessonView.y = 0
       lessonView.opacity = 1
    }
}
