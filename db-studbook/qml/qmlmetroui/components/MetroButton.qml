import QtQuick 1.1

Rectangle {
    property string btnColor:        "#04A7FF"
    property string headerText:      "Header"
    property string bodyText:        ""
    property string buttonIcon:      ""
    property string originIcon:      ""
    property bool   triggeredstate:  false
    property int    originX
    property int    originY
    property int    buttonInGroupe:  0
    signal clickRecived

    id: base
    width: 180
    height: 85
    color: btnColor

    Behavior on x {
        NumberAnimation {duration: 200}
    }

    Behavior on y {
        NumberAnimation {duration: 200}
    }

    Component.onCompleted: {
        originX = base.x
        originY = base.y
        originIcon = buttonIcon
    }

    Text {
        id: headText
        y: 6
        x: 6
        text: qsTr(headerText)
        color: "white"
        font.family: "Ubuntu"
        font.pixelSize: 16
        font.bold: true

    }

    Text{
        id: bodyTxt
        y: 6 + headText.height
        x: 6
        text: qsTr(bodyText)
        font.pixelSize: 10
        font.family: "Ubuntu"
        color: "white"
    }

    Image {
        id: image
        x: base.width - image.height - 6
        y: (base.height - image.height) / 2
        scale: 1
        source: buttonIcon
    }

    onTriggeredstateChanged: {
        if(triggeredstate != true){
            base.x = originX
            base.y = originY
            buttonIcon = originIcon
            image.scale = 1
        }
        else{
            buttonIcon = "qrc:/pics/pictures/cancel.png"
        }

    }

    MouseArea{
        anchors.fill: base


        onClicked: {
            base.triggeredstate = base.triggeredstate == true ? false : true
            clickRecived()
        }
    }
}
