// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item{

    property string exampleText: ""

    id: base
    width: parent.width - 20
    height: base_rec_text.paintedHeight + 20

    Rectangle{
        id: base_rec
        anchors.top: base.top
        anchors.left: base.left
        anchors.right: base.right
        anchors.bottom: base.bottom
        color: "darkgrey"

        Text{
            id: base_rec_text
            anchors.top: base_rec.top
            anchors.bottom: base_rec.bottom
            anchors.left: base_rec.left
            anchors.right: base_rec.right
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            //horizontalAlignment: Text.Right
            font.pixelSize: 20

            text: exampleText
        }
    }
}

