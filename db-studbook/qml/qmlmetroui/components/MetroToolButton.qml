// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    property string btnColor:        "#CD3278"
    property string headerText:      "Header"
    property string buttonIcon:      ""
    property int    originX
    property int    originY
    property int    buttonInGroupe:  0
    signal clickRecived

    id: buttonBody
    width: 180
    height: 85
    color: btnColor

    Behavior on opacity{
        NumberAnimation{duration: 200}
    }

    Text{
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.verticalCenter: parent.verticalCenter

        text: headerText
        color: "white"
        font.family: "Monaco"
        font.pixelSize: 20
        font.bold: true

    }

    Image{
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.verticalCenter: parent.verticalCenter
        source: buttonIcon
    }
}
