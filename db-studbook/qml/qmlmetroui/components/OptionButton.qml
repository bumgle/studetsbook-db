// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {

    property int number: 0
    property string variantText: "text"
    property string answerValue: "0"

    id: optionButtonBase
    width: parent.width - 30
    height: variantTextBase.paintedHeight + variantTextBase / 8
    color: "lightgrey"

    Text{
        id: variantTextBase
        text:variantText
        width: parent.width
        color: "black"
        wrapMode: Text.WordWrap
        textFormat: Text.StyledText
        horizontalAlignment: Text.AlignJustify
        font.pixelSize: 36
    }

}
