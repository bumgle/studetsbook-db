#include "database.h"
#include <QDebug>
#include <QFile>
#include <QtSql>
#include <QVariant>

Database::Database(QObject *parent) :
    QObject(parent)
{
    DATABASE_NAME = "content.sqlite";
}

void Database::initDatabase()
{
    if(checkDatabaseFileExistance()){
        connectToDatabase();
        qDebug() << "Database " + DATABASE_NAME + " is open!";
    }
    else{
        qDebug() << "Database file: " + DATABASE_NAME + "is missing!";
    }
}

bool Database::checkDatabaseFileExistance()
{
    QFile file(DATABASE_NAME);
    return file.exists();
}

void Database::connectToDatabase()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(DATABASE_NAME);
    db.open();
}

int Database::getLecturesCount()
{
    QSqlQuery * query = new QSqlQuery;
    query->exec("SELECT count(id) FROM lectures WHERE type = 'lecture';");
    query->next();
    return query->value(0).toInt();
}

int Database::getPracticesCount()
{
    QSqlQuery * query = new QSqlQuery;
    query->exec("SELECT count(id) FROM lectures WHERE type = 'practice';");
    query->next();
    return query->value(0).toInt();
}

int Database::getSelfJobCount()
{
    QSqlQuery * query = new QSqlQuery;
    query->exec("SELECT count(id) FROM lectures WHERE type = 'self';");
    query->next();
    return query->value(0).toInt();
}

QString Database::getStatisticsMainPage()
{
    QSqlQuery * query = new QSqlQuery;
    query->exec("SELECT sum(ball) FROM lectures;");
    query->next();

    return query->value(0).toString();
}

QStringList Database::getStatistics()
{
    QStringList params;

    QSqlQuery * query = new QSqlQuery;
    query->exec("SELECT count(id) FROM lectures;");
    query->next();

    params.append(query->value(0).toString()); // all items

    query->exec("SELECT count(id) FROM lectures GROUP BY type;");

    while(query->next()){
        params.append(query->value(0).toString()); // three params -- lectures count; practice count; self count;
    }

    query->exec("SELECT sum(pass) FROM lectures GROUP BY type;");

    while(query->next()){
        params.append(query->value(0).toString()); // three params -- lectures passed; practice passed; self passed;
    }

    query->exec("SELECT sum(ball) FROM lectures GROUP BY type;");

    while(query->next()){
        params.append(query->value(0).toString()); // three params -- lectures balls collected; practice balls collected; self balls collected;
    }

    qDebug() << params;

    return params;
}



QStringList Database::getLectureAtributes(QVariant id)
{
    int idInt = id.toInt();
    QSqlQuery *query = new QSqlQuery;
    query->prepare("SELECT * FROM lectures WHERE number = :id and type=\"lecture\"");
    query->bindValue(":id", idInt);
    query->exec();
    query->next();

   QStringList lecture;
   lecture.append(query->value(0).toString());
   lecture.append(query->value(1).toString());
   lecture.append(query->value(2).toString());
   lecture.append(query->value(3).toString());
   lecture.append(query->value(4).toString());
   lecture.append(query->value(5).toString());

   return lecture;
}

QStringList Database::getPracticeAtributes(QVariant id)
{
    int idInt = id.toInt();
    QSqlQuery *query = new QSqlQuery;
    query->prepare("SELECT * FROM lectures WHERE number = :id and type=\"practice\"");
    query->bindValue(":id", idInt);
    query->exec();
    query->next();

    QStringList practice;
    practice.append(query->value(0).toString());
    practice.append(query->value(1).toString());
    practice.append(query->value(2).toString());
    practice.append(query->value(3).toString());
    practice.append(query->value(4).toString());
    practice.append(query->value(5).toString());

    return practice;
}

QStringList Database::getSelfJobAtributes(QVariant id)
{
    int idInt = id.toInt();
    QSqlQuery *query = new QSqlQuery;
    query->prepare("SELECT * FROM lectures WHERE number = :id and type=\"self\"");
    query->bindValue(":id", idInt);
    query->exec();
    query->next();

    QStringList practice;
    practice.append(query->value(0).toString());
    practice.append(query->value(1).toString());
    practice.append(query->value(2).toString());
    practice.append(query->value(3).toString());
    practice.append(query->value(4).toString());
    practice.append(query->value(5).toString());

    return practice;
}

void Database::setEvaluateUnit(QVariant type, QVariant number, QVariant type_next, QVariant number_next, QVariant mark, QVariant status)
{
    QSqlQuery *query = new QSqlQuery();
    query->prepare("UPDATE lectures SET ball= :mark, pass = :status WHERE type = :type and number = :number;");
    query->bindValue(":mark", mark.toString());
    query->bindValue(":status", status.toString());
    query->bindValue(":type", type.toString());
    query->bindValue(":number", number.toString());
    query->exec();
    qDebug() << query->lastError();
    qDebug() << query->last();
    query->clear();

    if (status.toString() != "0"){
        query->prepare("UPDATE lectures SET status = :status WHERE type = :type and number = :number;");
        query->bindValue(":status", status.toString());
        query->bindValue(":type", type_next.toString());
        query->bindValue(":number", number_next.toString());
        query->exec();

        qDebug() << query->lastError();
        qDebug() << query->last();
    }
}
