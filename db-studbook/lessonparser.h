#ifndef LESSONPARSER_H
#define LESSONPARSER_H

#include <QObject>
#include <QVariantList>

class LessonParser : public QObject
{
    Q_OBJECT
public:
    explicit LessonParser(QObject *parent = 0);

private:
    QVariantList items;
    void parseText(QString str);
    int readTagName(int pos, QString str);
    int readTagBody(int pos, QString str, QString tag);
    QStringList readQuestion(QString str);
    QString readQuestionVariant(int * p, QString s);
    
public slots:
    Q_INVOKABLE  QVariantList setLecture(QVariant lesson);
};

#endif // LESSONPARSER_H
