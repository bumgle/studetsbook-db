#include "lessonparser.h"
#include <QVariantList>
#include <QStringList>
#include <QFile>
#include <QDebug>

LessonParser::LessonParser(QObject *parent) :
    QObject(parent)
{
    //qDebug() << setLecture(":lectures/lectures/lesson1.txt");
}

QVariantList LessonParser::setLecture(QVariant lesson)
{
    QFile file(lesson.toString()/*":lectures/lectures/lesson1.txt"*/);

    if(!file.open(QIODevice::ReadOnly))
        qDebug() << "Error open file " << file.fileName();

    QTextStream in(&file);
    QString str = in.readAll();

    parseText(str);
    qDebug() << items;
    return items;
}

void LessonParser::parseText(QString str)
{
    for(int i = 0; i < str.length(); i++){
        QString part = str.mid(i, 3);

        if(part == "###"){
            i = readTagName(i + 2, str);
        }
    }
}



int LessonParser::readTagName(int pos, QString str)
{
    QString tag;
    QString character = "";

    while(character != "<"){
        tag += character;
        pos++;
        character = str.mid(pos, 1);
    }
    pos = readTagBody(pos, str, tag);

    return pos;
}

int LessonParser::readTagBody(int pos, QString str, QString tag)
{
    QString body;
    QString character = "";
    QString characterStop = "";

    while(characterStop != ">>>"){
        body += character;
        pos++;
        character = str.mid(pos, 1);
        characterStop = str.mid(pos, 3);
    }


    if(tag == "TEXT"){

        QStringList content;
        content << tag << body;
        items.append(content);
    }

    if(tag == "HEADER"){
        QStringList content;
        content << tag << body;
        items.append(content);
    }

    if(tag == "IMAGE"){
        QStringList content;
        content << tag << body;
        items.append(content);
    }

    if(tag == "QSINGLE"){
        QStringList content;
        content << tag;
        //qDebug() << body;
        content.append(readQuestion(body));
        items.append(content);
        //qDebug() << content;
    }

    return pos;
}

QStringList LessonParser::readQuestion(QString str)
{
    int pos = 0;

    const QString tagIdBegin = "<id>";
    const QString tagIdEnd = "</id>";

    // read question id

    while (str.mid(pos, tagIdBegin.length()) != tagIdBegin){
        pos++;
    }

    pos += tagIdBegin.length();
    QString QuestionId;

    while (str.mid(pos, tagIdEnd.length()) != tagIdEnd){
        QuestionId += str.mid(pos, 1);
        pos++;
    }

    //qDebug() << QuestionId;
    pos += tagIdEnd.length();

    // read question body

    const QString questionBodyBegin = "<question>";
    const QString questionBodyEnd = "</question>";

    while(str.mid(pos, questionBodyBegin.length()) != questionBodyBegin){
        pos++;
    }

    pos += questionBodyBegin.length();
    QString QuestionBody;

    while(str.mid(pos, questionBodyEnd.length()) != questionBodyEnd){
        QuestionBody += str.mid(pos, 1);
        pos++;
    }

    pos += questionBodyEnd.length();

    // reading variants

    QStringList variants;

    while (str.length() != (pos + 1)) {
        variants << readQuestionVariant(&pos, str);
    }
    //qDebug() << QuestionBody;
    //qDebug() << variants;

    QStringList question;
    question << QuestionId
             << QuestionBody;
    question.append(variants);

    return question;
}

QString LessonParser::readQuestionVariant(int *p, QString s)
{
    const QString variantBegining = "<variant ";
    const QString variantEnds     = "</variant>";
    const QString variantNum      = "num=";
    const QString variantVal      = "val=";

    while (s.mid(*p, variantBegining.length()) != variantBegining) {
        *p += 1;
    }

    *p += variantBegining.length();

    while (s.mid(*p, variantNum.length()) != variantNum) {
        *p += 1;
    }

    *p += variantNum.length();
    QString variantNumber;

    while (s.mid(*p, variantVal.length()) != variantVal) {
        variantNumber += s.mid(*p, 1);
        *p += 1;
    }

    variantNumber.remove(" ");
    *p += variantVal.length();
    QString variantValue;

    while (s.mid(*p, 1) != ">") {
        variantValue += s.mid(*p, 1);
        *p += 1;
    }

    variantValue.remove(" ");
    *p += 1;
    QString variantBody;

    while (s.mid(*p, variantEnds.length()) != variantEnds) {
        variantBody += s.mid(*p, 1);
        *p += 1;
    }

    *p += variantEnds.length();

    return variantNumber + "##" + variantValue + "##" + variantBody;
}
