#ifndef BRIDGE_H
#define BRIDGE_H

#include <QObject>

class Bridge : public QObject
{
    Q_OBJECT
public:
    explicit Bridge(QObject *parent = 0);

    Q_INVOKABLE int screeneWidth();
    Q_INVOKABLE int screeneHeight();
    
signals:
    
public slots:
    
};

#endif // BRIDGE_H
