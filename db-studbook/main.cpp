#include <QtGui/QApplication>
#include <qdeclarative.h>
#include "lessonparser.h"
#include "qmlapplicationviewer.h"
#include "bridge.h"
#include "line.h"
#include "database.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    //LessonParser *parser = new LessonParser;

    qmlRegisterType<Bridge>("Bridge",1,0,"Bridge");
    qmlRegisterType<Database>("Bridge", 1, 0, "Database");
    qmlRegisterType<LessonParser>("Bridge", 1, 0, "Parser");
    qmlRegisterType<Line>("CustomComponents", 1, 0, "Line");

    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/qmlmetroui/main.qml"));
    viewer.showFullScreen();
    //viewer.show();

    return app->exec();
}
