#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QVariant>
#include <QStringList>

class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = 0);

private:
    QString DATABASE_NAME;
    bool checkDatabaseFileExistance();
    void connectToDatabase();

signals:

public slots:
    Q_INVOKABLE void initDatabase();
    Q_INVOKABLE int getLecturesCount();
    Q_INVOKABLE int getPracticesCount();
    Q_INVOKABLE int getSelfJobCount();
    Q_INVOKABLE QString getStatisticsMainPage();
    Q_INVOKABLE QStringList getStatistics();
    Q_INVOKABLE QStringList getLectureAtributes(QVariant id);
    Q_INVOKABLE QStringList getPracticeAtributes(QVariant id);
    Q_INVOKABLE QStringList getSelfJobAtributes(QVariant id);
    Q_INVOKABLE void setEvaluateUnit(QVariant type, QVariant number, QVariant type_next, QVariant number_next, QVariant mark, QVariant status);

};

#endif // DATABASE_H
